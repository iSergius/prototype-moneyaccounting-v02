/**
 * Created by isergius on 28.07.16.
 */
import React from 'react';
import ReactDOM from 'react-dom'
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { createAction, handleAction, handleActions } from 'redux-actions';
import { Router, Route, Link, browserHistory } from 'react-router'


class ControlsWidget extends React.Component {
    render() {
        return (
            <section className="accounts_bar">
                <header className="section_header">
                    <h1 className="section_title">MANAGE</h1>
                </header>
                <ul>
                    <li><Link to="/transactions">Transactions</Link></li>
                </ul>
            </section>
        );
    }
}

export default ControlsWidget