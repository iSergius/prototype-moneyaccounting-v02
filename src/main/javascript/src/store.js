/**
 * Created by isergius on 24.07.16.
 */
import { createStore, applyMiddleware, compose, combineReducers } from 'redux'
import traverson from 'traverson'
import JsonHalAdapter from 'traverson-hal'

import { accounts } from './accountsWidget.jsx'
import { transactions } from './transactionsView.jsx'


traverson.registerMediaType(JsonHalAdapter.mediaType, JsonHalAdapter);

const walker = store => next => action => {
    if (action.payload.path) {
        traverson
            .from('/api/')
            .jsonHal()
            .follow(action.payload.path)
            .withRequestOptions({
                headers: {
                    'Authorization': 'Basic YWRtaW5AaXNlcmdpdXMubmFtZTpwYXNzd29yZA=='
                }
            })
            .getResource((error, document) => {
                if (error) {
                    next(Object.assign(action, {error: error}));
                } else {
                    next(Object.assign(action, {payload: document}));
                }
            });
    } else {
        next(action)
    }
};

const store = createStore(
    combineReducers({
        accounts,
        transactions
    }),
    applyMiddleware(
        walker
    )
);

export default store;

