import React from 'react'

class FooterBar extends React.Component {
    render() {
        return (
            <footer className="copyrigth">
                <small>&copy; iSergius 2016</small>
            </footer>
        );
    }
}

export default FooterBar