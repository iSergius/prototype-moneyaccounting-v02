/**
 * Created by isergius on 29.07.16.
 */
import React from 'react';
import ReactDOM from 'react-dom'
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { createAction, handleAction, handleActions } from 'redux-actions';

const TRANSACTIONS_PATH = "app:transactions";

const getTransactions = createAction('transactions', ()=> {
    return {
        path: ["app:home", TRANSACTIONS_PATH]
    }
});

const transactions = handleAction(getTransactions, {
    next(state, action) {
        return action.payload._embedded[TRANSACTIONS_PATH];
    }
},[
    {
        "transactionId": 1,
        "value": {
            "amount": "",
            "currencyCode": ""
        },
        "srcAccount": null,
        "dstAccount": {
            "accountId": 1,
            "title": ""
        },
        "date": null,
        "committed": false
    }
]);

const TransactionsView = connect(
    (state)=> {
        if (state.transactions instanceof Array) {
            return {
                transactions: state.transactions
        }
        } else {
            return {
                transactions: new Array(state.transactions)
            }
        }
    },
    (dispatch)=> {
        return bindActionCreators({getTransactions}, dispatch)
    }
)(React.createClass({
    componentWillMount() {
        this.props.getTransactions()
    },
    render() {
        return (
            <div>
                <header className="section_header">
                    <h1 className="section_title">TRANSACTIONS</h1>
                </header>
                <table>
                    <thead>
                    <tr>
                        <th>Record</th>
                        <th>Date</th>
                        <th>Amount</th>
                        <th>Currency</th>
                    </tr>
                    </thead>
                    <tbody>
                    {this.props.transactions.map((item)=> {
                        return (
                            <tr key={item.transactionId}>
                                <td>{item.committed}</td>
                                <td>{item.date}</td>
                                <td>{item.value.amount}</td>
                                <td>{item.value.currencyCode}</td>
                            </tr>
                        );
                    })}
                    </tbody>
                </table>
            </div>
        );
    }
}));

export {getTransactions, TransactionsView, transactions}