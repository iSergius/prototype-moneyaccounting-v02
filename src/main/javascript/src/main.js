import React from 'react'
import ReactDOM from 'react-dom'
import {hashHistory, Router, Route} from 'react-router'
import {Provider} from 'react-redux'

import WorkLayout from './layouts/workLayout.jsx'
import ChildDelegateLayout from './layouts/childDelegateLayout.jsx'
import HeaderWidget from './headerWidget.jsx'
import {AccountsWidget} from './accountsWidget.jsx'
import ControlsWidget from './controlsWidget.jsx'
import CopyWidget from './copyWidget.jsx'
import {TransactionsView} from './transactionsView.jsx'
import store from './store'

class App extends React.Component {
    render() {
        return (
            <Provider store={store}>
                <Router history={hashHistory}>
                    <Route component={WorkLayout}>
                        <Route path="/" components={{
                                header: HeaderWidget,
                                head_bar: AccountsWidget,
                                middle_bar: ControlsWidget,
                                footer_bar: CopyWidget,
                                center: ChildDelegateLayout}}>
                            <Route path="/transactions" component={TransactionsView} />
                        </Route>
                    </Route>
                </Router>
            </Provider>
        );
    }
}

ReactDOM.render(<App />, document.getElementById("app"));
