import React from 'react';
import ReactDOM from 'react-dom'
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { createAction, handleAction, handleActions } from 'redux-actions';


const getAccounts = createAction('accounts', () => {
    return {
        path: ['app:home', 'app:accounts', 'app:accounts']
    }
});

const accounts = handleAction(getAccounts, {
    next(state, action) {
        return action.payload;
    }
}, [{accountId: 1, title: " ", value: {amount: " ", currencyCode: " "}}]);

const AccountsWidget = connect(
    (state) => {
        if (state.accounts instanceof Array) {
            return {
                accounts: state.accounts
            }
        } else {
            return {
                accounts: new Array(state.accounts)
            }
        }
    },
    (dispatch) => {
        return bindActionCreators({getAccounts}, dispatch)
    }
)(React.createClass({
    componentWillMount() {
        this.props.getAccounts()
    },
    render() {
        return (
            <section className="accounts_bar">
                <header className="section_header">
                    <h1 className="section_title">ACCOUNTS</h1>
                </header>
                <div>
                    {this.props.accounts.map((item) => {
                        return (
                            <article key={item.accountId}>
                                <h1 onClick={()=>{this.props.getAccounts()}}>{item.title}</h1>
                                <span>{item.value.amount}</span>
                                <span>{item.value.currencyCode}</span>
                            </article>
                        );
                    })}
                </div>
            </section>
        );
    }
}));
/**
 * @export accounts - reducer
 * @export AccountsView - React element
 * @export getAccounts - action creator
 * */
export { accounts, AccountsWidget, getAccounts }
