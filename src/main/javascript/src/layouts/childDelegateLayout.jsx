import React from 'react'

class ChildDelegateLayout extends React.Component {
    render() {
        return (
            <div>
                {this.props.children}
            </div>
        );
    }
}

export default ChildDelegateLayout