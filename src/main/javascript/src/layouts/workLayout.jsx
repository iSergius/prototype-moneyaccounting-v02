import React from 'react'


class WorkLayout extends React.Component {
    render() {
        return (
            <div>
                <header className="title_bar">
                    {this.props.header}
                </header>
                <div className="bar">
                    {this.props.head_bar}
                    {this.props.middle_bar}
                    {this.props.footer_bar}
                </div>
                <section className="center_bar">
                    {this.props.center}
                </section>
            </div>
        );
    }
}

export default WorkLayout