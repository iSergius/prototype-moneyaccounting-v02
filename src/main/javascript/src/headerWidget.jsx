import ReactDOM from 'react-dom'
import React from 'react'
import { Router, Route, Link, browserHistory } from 'react-router'

class HeaderView extends React.Component {
    render() {
        return (
            <ul>
                <li><Link to="/">Home</Link></li>
            </ul>
        );
    }
}

export default HeaderView

