package name.isergius.personal.money.account.dao;


import name.isergius.personal.money.account.domain.Transaction;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

/**
 * Created by isergius on 28.03.16.
 */
public interface TransactionDao extends PagingAndSortingRepository<Transaction, Long> {

    Page<Transaction> committedTrue(Pageable pageable);

    Page<Transaction> committedFalse(Pageable pageable);

    @Query("SELECT t FROM Transaction t WHERE t.agent = ?#{ principal?.agent }")
    Page<Transaction> findAllPersonal(Pageable pageable);

    @Query("SELECT t FROM Transaction t WHERE t.agent = ?#{ principal?.agent } AND t.id = ?1")
    Transaction findOnePersonal(Long id);

    @Query("DELETE FROM Transaction t WHERE t.agent = ?#{ principal?.agent } AND t.id = ?1")
    void deletePersonal(Long id);

    @Query("SELECT t FROM Transaction t WHERE t.agent = ?#{ principal?.agent } AND t.committed = true ")
    Page<Transaction> committedTruePersonal(Pageable pageable);

    @Query("SELECT t FROM Transaction t WHERE t.agent = ?#{ principal?.agent } AND t.committed = false ")
    Page<Transaction> committedFalsePersonal(Pageable pageable);
}
