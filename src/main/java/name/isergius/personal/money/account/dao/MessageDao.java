package name.isergius.personal.money.account.dao;

import name.isergius.personal.money.account.domain.Message;
import org.springframework.data.repository.PagingAndSortingRepository;

import java.util.List;

/**
 * Created by isergius on 19.06.16.
 */
public interface MessageDao extends PagingAndSortingRepository<Message, Long> {
    List<Message> transmittedIsFalse();
}
