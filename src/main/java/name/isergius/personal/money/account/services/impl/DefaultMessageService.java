package name.isergius.personal.money.account.services.impl;

import name.isergius.personal.money.account.dao.MessageDao;
import name.isergius.personal.money.account.domain.Message;
import name.isergius.personal.money.account.services.MessageService;
import name.isergius.personal.money.account.services.messag.MessageBuilder;
import name.isergius.personal.money.account.services.messag.MessageBuilderFactory;
import name.isergius.personal.money.account.services.messag.MessageDeliveryService;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * Created by isergius on 17.06.16.
 */
@Transactional(propagation = Propagation.REQUIRES_NEW, isolation = Isolation.REPEATABLE_READ)
public class DefaultMessageService implements MessageService {

    private final MessageBuilderFactory messageBuilderFactory;
    private List<MessageDeliveryService> messageDeliveryServices;
    private MessageDao messageDao;

    public DefaultMessageService(MessageBuilderFactory messageBuilderFactory,
                                 List<MessageDeliveryService> messageDeliveryServices,
                                 MessageDao messageDao) {
        this.messageBuilderFactory = messageBuilderFactory;
        this.messageDeliveryServices = messageDeliveryServices;
        this.messageDao = messageDao;
    }

    @Override
    public void send(Message message) {
        message.setTransmitted(false);
        messageDao.save(message);
    }

    @Override
    public void transmitAll() {
        List<Message> messages = messageDao.transmittedIsFalse();
        messages.parallelStream().forEach(this::transmit);
    }

    private void transmit(Message message) {
        messageDeliveryServices.forEach(messageDeliveryService ->
                messageDeliveryService.delivery(message));
        messageDao.save(message);
    }

    @Override
    public MessageBuilder builder() {
        return messageBuilderFactory.factory();
    }
}
