package name.isergius.personal.money.account.services.impl;

import name.isergius.personal.money.account.dao.RoleDao;
import name.isergius.personal.money.account.domain.*;
import name.isergius.personal.money.account.services.UserBuilder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;
import java.util.UUID;

/**
 * Created by isergius on 21.06.16.
 */
@Transactional(propagation = Propagation.MANDATORY, isolation = Isolation.SERIALIZABLE)
public class DefaultUserBuilder implements UserBuilder {

    public static final String PROPERTY_ENABLED = "enabled";
    public static final String PROPERTY_CONFIRMED = "confirmed";
    public static final String PROPERTY_ACCOUNT_EXPIRED = "accountExpired";
    public static final String PROPERTY_ACCOUNT_NON_LOCKED = "accountNonLocked";
    public static final String PROPERTY_CREDENTIALS_EXPIRED = "credentialsExpired";
    public static final String PROPERTY_AGENT_TITLE = "agentTitle";
    public static final String PROPERTY_ACCOUNT_TITLE = "accountTitle";
    public static final String PROPERTY_ROLE = "role";
    public static final String PROPERTY_CURRENCY = "currency";
    public static final String PROPERTY_VALUE = "value";
    public static final String PROPERTY_OVERDRAFT = "overdraft";

    private final RoleDao roleDao;
    private final PasswordEncoder passwordEncoder;

    private String username;
    private String password;
    private List<Role> roles = new ArrayList<>();

    private boolean enabled;
    private boolean accountNonLocked;
    private boolean confirmed;
    private LocalDateTime accountExpired;
    private LocalDateTime credentialsExpired;
    private String secret;

    private String agentTitle;
    private List<Account> accounts = new ArrayList<>();


    public DefaultUserBuilder(RoleDao roleDao,
                              PasswordEncoder passwordEncoder,
                              Properties defaultProperties) {
        this.roleDao = roleDao;
        this.passwordEncoder = passwordEncoder;

        this.enabled = (Boolean) defaultProperties.get(PROPERTY_ENABLED);
        this.accountNonLocked = (Boolean) defaultProperties.get(PROPERTY_ACCOUNT_NON_LOCKED);
        this.confirmed = (Boolean) defaultProperties.get(PROPERTY_CONFIRMED);
        this.accountExpired = (LocalDateTime) defaultProperties.get(PROPERTY_ACCOUNT_EXPIRED);
        this.credentialsExpired = (LocalDateTime) defaultProperties.get(PROPERTY_CREDENTIALS_EXPIRED);
        this.secret = UUID.randomUUID().toString();
        this.agentTitle = defaultProperties.getProperty(PROPERTY_AGENT_TITLE);
        addRole(defaultProperties.getProperty(PROPERTY_ROLE));
        addAccount(defaultProperties.getProperty(PROPERTY_ACCOUNT_TITLE),
                (BigDecimal) defaultProperties.get(PROPERTY_VALUE),
                defaultProperties.getProperty(PROPERTY_CURRENCY),
                (Boolean) defaultProperties.get(PROPERTY_OVERDRAFT));
    }

    @Override
    public UserBuilder setUsername(String username) {
        this.username = username;
        return this;
    }

    @Override
    public UserBuilder setPassword(String password) {
        this.password = password;
        return this;
    }

    @Override
    public UserBuilder setEnabled(boolean enabled) {
        this.enabled = enabled;
        return this;
    }

    @Override
    public UserBuilder setAccountNonLocked(boolean accountNonLocked) {
        this.accountNonLocked = accountNonLocked;
        return this;
    }

    @Override
    public UserBuilder setConfirmed(boolean confirmed) {
        this.confirmed = confirmed;
        return this;
    }

    @Override
    public UserBuilder setAccountExpired(LocalDateTime accountExpired) {
        this.accountExpired = accountExpired;
        return this;
    }

    @Override
    public UserBuilder setCredentialsExpired(LocalDateTime credentialsExpired) {
        this.credentialsExpired = credentialsExpired;
        return this;
    }

    @Override
    public UserBuilder setSecret(String secret) {
        this.secret = secret;
        return this;
    }

    @Override
    public UserBuilder setAgentTitle(String agentTitle) {
        this.agentTitle = agentTitle;
        return this;
    }

    @Override
    public UserBuilder addRole(String title) {
        Role role = roleDao.findByTitle(title);
        this.roles.add(role);
        return this;
    }

    @Override
    public UserBuilder addAccount(String title, BigDecimal value, String currency, boolean overdraft) {
        Money money = new Money(value, currency);
        Account account = new Account(title, overdraft, money);
        this.accounts.add(account);
        return this;
    }

    @Override
    public User build() {
        Agent agent = buildAgent();
        User user = new User();
        user.setUsername(this.username);
        user.setPassword(this.passwordEncoder.encode(this.password));
        user.setEnabled(this.enabled);
        user.setAccountExpired(this.accountExpired);
        user.setAccountNonLocked(this.accountNonLocked);
        user.setConfirmed(this.confirmed);
        user.setCredentialsExpired(this.credentialsExpired);
        user.setRoles(this.roles);
        user.setSecret(this.secret);
        user.setAgent(agent);
        return user;
    }

    private Agent buildAgent() {
        Agent agent = new Agent();
        agent.setTitle(this.agentTitle);
        agent.setAccounts(this.accounts);
        return agent;
    }

}
