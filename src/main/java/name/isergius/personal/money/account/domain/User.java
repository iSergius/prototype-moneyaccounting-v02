package name.isergius.personal.money.account.domain;

import name.isergius.personal.money.account.dao.Model;
import org.springframework.security.core.CredentialsContainer;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import javax.persistence.*;
import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.Collection;
import java.util.List;
import java.util.Objects;

/**
 * Created by isergius on 21.03.16.
 */
@Entity
@Table(name = "USERS")
public class User extends Model implements Serializable, UserDetails, CredentialsContainer {

    private static final long serialVersionUID = -6544295172901159653L;

    private Agent agent;
    private List<Agent> contragents;

    private String username;
    private String password;
    private List<Role> roles;
    private LocalDateTime accountExpired;
    private LocalDateTime credentialsExpired;
    private boolean locked;
    private boolean enabled;
    private boolean confirmed;
    private String secret;

    @OneToOne(cascade = CascadeType.ALL, fetch = FetchType.EAGER)
    public Agent getAgent() {
        return agent;
    }

    public void setAgent(Agent agent) {
        this.agent = agent;
    }

    @ManyToMany(cascade = CascadeType.ALL, fetch = FetchType.EAGER)
    public List<Agent> getContragents() {
        return contragents;
    }

    public void setContragents(List<Agent> contragents) {
        this.contragents = contragents;
    }

    @Basic
    @Column(name = "username", unique = true)
    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    @Basic
    @Column(name = "password")
    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    @ManyToMany(fetch = FetchType.EAGER)
    @JoinTable(name = "users_roles",
            joinColumns = @JoinColumn(name = "credentials_id"),
            inverseJoinColumns = @JoinColumn(name = "roles_id"))
    public List<Role> getRoles() {
        return roles;
    }

    public void setRoles(List<Role> roles) {
        this.roles = roles;
    }

    @Transient
    @Override
    public boolean isAccountNonExpired() {
        if (accountExpired == null) return true;
        else return LocalDateTime.now().isBefore(accountExpired);
    }

    public void setAccountNonLocked(boolean locked) {
        this.locked = locked;
    }

    @Basic
    @Column(name = "accountnonlocked")
    @Override
    public boolean isAccountNonLocked() {
        return locked;
    }

    @Transient
    @Override
    public boolean isCredentialsNonExpired() {
        if (credentialsExpired == null) return true;
        else return LocalDateTime.now().isBefore(credentialsExpired);
    }

    public void setEnabled(boolean enabled) {
        this.enabled = enabled;
    }

    @Basic
    @Column(name = "enabled")
    @Override
    public boolean isEnabled() {
        return enabled;
    }

    @Basic
    @Column(name = "confirmed")
    public boolean isConfirmed() {
        return confirmed;
    }

    public void setConfirmed(boolean confirmed) {
        this.confirmed = confirmed;
    }

    @Transient
    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        return roles;
    }

    @Override
    public void eraseCredentials() {
        this.password = null;
    }

    @Basic
    @Column(name = "accountexpired")
    public LocalDateTime getAccountExpired() {
        return accountExpired;
    }

    public void setAccountExpired(LocalDateTime accountExpired) {
        this.accountExpired = accountExpired;
    }

    @Basic
    @Column(name = "credentialsexpired")
    public LocalDateTime getCredentialsExpired() {
        return credentialsExpired;
    }

    public void setCredentialsExpired(LocalDateTime credentialsExpired) {
        this.credentialsExpired = credentialsExpired;
    }

    @Basic
    @Column(name = "secret")
    public String getSecret() {
        return secret;
    }

    public void setSecret(String secret) {
        this.secret = secret;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;
        User user = (User) o;
        return locked == user.locked &&
                enabled == user.enabled &&
                confirmed == user.confirmed &&
                Objects.equals(agent, user.agent) &&
                Objects.equals(contragents, user.contragents) &&
                Objects.equals(username, user.username) &&
                Objects.equals(password, user.password) &&
                Objects.equals(roles, user.roles) &&
                Objects.equals(accountExpired, user.accountExpired) &&
                Objects.equals(credentialsExpired, user.credentialsExpired) &&
                Objects.equals(secret, user.secret);
    }

    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(), agent, contragents, username, password, roles, accountExpired, credentialsExpired, locked, enabled, confirmed, secret);
    }

    @Override
    public String toString() {
        return "User{" +
                "agent=" + agent +
                ", contragents=" + contragents +
                ", username='" + username + '\'' +
                ", password='" + password + '\'' +
                ", roles=" + roles +
                ", accountExpired=" + accountExpired +
                ", credentialsExpired=" + credentialsExpired +
                ", locked=" + locked +
                ", enabled=" + enabled +
                ", confirmed=" + confirmed +
                ", secret='" + secret + '\'' +
                '}';
    }
}
