package name.isergius.personal.money.account.services.messag;

import org.apache.velocity.app.VelocityEngine;

import java.util.Properties;

/**
 * Created by isergius on 27.06.16.
 */
public class MessageBuilderFactory {

    private VelocityEngine velocityEngine;
    private Properties defaultProperties;

    public MessageBuilderFactory(VelocityEngine velocityEngine, Properties defaultProperties) {
        this.velocityEngine = velocityEngine;
        this.defaultProperties = defaultProperties;
    }

    public MessageBuilder factory() {
        return new DefaultMessageBuilder(velocityEngine, defaultProperties);
    }
}
