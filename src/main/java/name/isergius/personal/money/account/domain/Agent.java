package name.isergius.personal.money.account.domain;

import name.isergius.personal.money.account.dao.Model;

import javax.persistence.*;
import java.io.Serializable;
import java.util.List;
import java.util.Objects;

/**
 * Created by isergius on 21.03.16.
 */
@Entity
@Table(name = "AGENTS")
public class Agent extends Model implements Serializable {

    private static final long serialVersionUID = -7406234439978869106L;

    private String title;
    private List<Account> accounts;

    @Basic
    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    @OneToMany(cascade = CascadeType.ALL, fetch = FetchType.EAGER)
    public List<Account> getAccounts() {
        return accounts;
    }

    public void setAccounts(List<Account> accounts) {
        this.accounts = accounts;
    }

    public void addAccount(Account account) {
        accounts.add(account);
    }

    public void removeAccount(Account account) {
        accounts.remove(account);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;
        Agent agent = (Agent) o;
        return Objects.equals(title, agent.title) &&
                Objects.equals(accounts, agent.accounts);
    }

    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode());
    }

    @Override
    public String toString() {
        return "Agent{" +
                "title='" + title + '\'' +
                ", accounts=" + accounts +
                '}';
    }
}
