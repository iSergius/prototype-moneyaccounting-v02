package name.isergius.personal.money.account.services.impl;

import name.isergius.personal.money.account.dao.Model;
import name.isergius.personal.money.account.dao.UserDao;
import name.isergius.personal.money.account.domain.User;
import name.isergius.personal.money.account.services.UserBuilder;
import name.isergius.personal.money.account.services.UserBuilderFactory;
import name.isergius.personal.money.account.services.UserService;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.Objects;

/**
 * Created by isergius on 16.06.16.
 */
@Transactional(propagation = Propagation.REQUIRES_NEW, isolation = Isolation.SERIALIZABLE)
public class DefaultUserService implements UserService {

    private UserBuilderFactory userBuilderFactory;
    private UserDao userDao;

    public DefaultUserService(UserDao userDao, UserBuilderFactory userBuilderFactory) {
        this.userDao = userDao;
        this.userBuilderFactory = userBuilderFactory;
    }

    @Override
    public User getCurrentUser() {
        Model model = (Model) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        return userDao.findOne(model.getId());
    }

    @Override
    public void save(User user) {
        userDao.save(user);
    }

    @Override
    public boolean currentUserIsContainRole(String roleName) {
        User user = getCurrentUser();
        return user.getRoles()
                .stream()
                .filter(role -> role.getTitle().equals(roleName))
                .count() == 1;
    }

    @Override
    public UserBuilder builder() {
        return userBuilderFactory.factory();
    }

    @Override
    public boolean confirmRegistration(String secret) {
        User credentials = userDao.secretIs(secret);
        if (Objects.nonNull(credentials)) {
            credentials.setConfirmed(true);
            return true;
        }
        return false;
    }
}
