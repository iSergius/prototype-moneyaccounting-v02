package name.isergius.personal.money.account.controller.dto;

import org.springframework.hateoas.ResourceSupport;
import org.springframework.hateoas.core.Relation;

import java.time.LocalDate;

/**
 * Created by isergius on 28.04.16.
 */
@Relation(value = TransactionResource.SINGLE_RELATION, collectionRelation = TransactionResource.COLLECTION_RELATION)
public class TransactionResource extends ResourceSupport {

    public static final String SINGLE_RELATION = "transaction";
    public static final String COLLECTION_RELATION = "transactions";

    private Long transactionId;
    private MoneyAmountResource value;
    private ListProjectionAccountResource srcAccount;
    private ListProjectionAccountResource dstAccount;
    private LocalDate date;
    private boolean committed;

    public Long getTransactionId() {
        return transactionId;
    }

    public void setTransactionId(Long transactionId) {
        this.transactionId = transactionId;
    }

    public MoneyAmountResource getValue() {
        return value;
    }

    public void setValue(MoneyAmountResource value) {
        this.value = value;
    }

    public ListProjectionAccountResource getSrcAccount() {
        return srcAccount;
    }

    public void setSrcAccount(ListProjectionAccountResource srcAccount) {
        this.srcAccount = srcAccount;
    }

    public ListProjectionAccountResource getDstAccount() {
        return dstAccount;
    }

    public void setDstAccount(ListProjectionAccountResource dstAccount) {
        this.dstAccount = dstAccount;
    }

    public LocalDate getDate() {
        return date;
    }

    public void setDate(LocalDate date) {
        this.date = date;
    }

    public boolean isCommitted() {
        return committed;
    }

    public void setCommitted(boolean committed) {
        this.committed = committed;
    }
}
