package name.isergius.personal.money.account.config;

import name.isergius.personal.money.account.controller.asm.AccountProcessor;
import name.isergius.personal.money.account.controller.asm.TransactionProcessor;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.web.config.EnableSpringDataWebSupport;
import org.springframework.hateoas.UriTemplate;
import org.springframework.hateoas.config.EnableEntityLinks;
import org.springframework.hateoas.config.EnableHypermediaSupport;
import org.springframework.hateoas.hal.CurieProvider;
import org.springframework.hateoas.hal.DefaultCurieProvider;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;

import javax.servlet.ServletContext;

/**
 * Created by isergius on 21.04.16.
 */
@Configuration
@EnableWebMvc
@EnableEntityLinks
@EnableSpringDataWebSupport
@EnableHypermediaSupport(type= {EnableHypermediaSupport.HypermediaType.HAL})
@ComponentScan(basePackages = "name.isergius.personal.money.account.controller")
public class WebConfig {

    @Bean
    public TransactionProcessor transactionProcessor() {
        return new TransactionProcessor();
    }

    @Bean
    public AccountProcessor accountProcessor() {
        return new AccountProcessor();
    }

    @Bean
    public CurieProvider curieProvider(ServletContext servletContext) {
        return new DefaultCurieProvider("app", new UriTemplate(servletContext.getContextPath() + "/rel/{rel}"));
    }
}
