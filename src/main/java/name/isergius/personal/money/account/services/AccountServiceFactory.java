package name.isergius.personal.money.account.services;

import name.isergius.personal.money.account.config.SecurityConfig;
import name.isergius.personal.money.account.dao.AccountDao;
import name.isergius.personal.money.account.services.impl.DefaultAccountService;
import name.isergius.personal.money.account.services.impl.PersonalAccountService;
import org.springframework.core.env.Environment;


/**
 * Created by isergius on 28.06.16.
 */
public class AccountServiceFactory {

    private final UserService userService;
    private final AccountDao accountDao;
    private final Environment environment;

    public AccountServiceFactory(AccountDao accountDao,
                                 UserService userService,
                                 Environment environment) {
        this.userService = userService;
        this.accountDao = accountDao;
        this.environment = environment;
    }

    public AccountService factory() {
        if (userService.currentUserIsContainRole(environment.getProperty(SecurityConfig.PROPERTY_ADMIN_ROLE)))
            return new DefaultAccountService(accountDao);
        else
            return new PersonalAccountService(userService);
    }
}
