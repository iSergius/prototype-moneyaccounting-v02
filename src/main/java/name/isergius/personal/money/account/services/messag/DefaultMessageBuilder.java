package name.isergius.personal.money.account.services.messag;

import name.isergius.personal.money.account.domain.Message;
import org.apache.velocity.app.VelocityEngine;
import org.springframework.ui.velocity.VelocityEngineUtils;

import java.util.Map;
import java.util.Properties;

/**
 * Created by isergius on 27.06.16.
 */
public class DefaultMessageBuilder implements MessageBuilder {

    public static final String PROPERTY_TITLE = "title";
    public static final String PROPERTY_ENCODING = "encoding";
    public static final String PROPERTY_TEMPLATE_PATH = "templatePath";
    public static final String PROPERTY_TEMPLATE_SUFFIX = "templateSuffix";

    private VelocityEngine velocityEngine;

    private String address;
    private String title;
    private String content;
    private String template;
    private Map<String, Object> params;
    private String encoding;
    private String templatePath;
    private String templateSuffix;

    public DefaultMessageBuilder(VelocityEngine velocityEngine, Properties defaultProperties) {
        this.velocityEngine = velocityEngine;
        this.title = defaultProperties.getProperty(PROPERTY_TITLE);
        this.encoding = defaultProperties.getProperty(PROPERTY_ENCODING);
        this.templatePath = defaultProperties.getProperty(PROPERTY_TEMPLATE_PATH);
        this.templateSuffix = defaultProperties.getProperty(PROPERTY_TEMPLATE_SUFFIX);
    }

    @Override
    public MessageBuilder setAddress(String address) {
        this.address = address;
        return this;
    }

    @Override
    public MessageBuilder setTitle(String title) {
        this.title = title;
        return this;
    }

    @Override
    public MessageBuilder setContent(String content) {
        this.content = content;
        return this;
    }

    @Override
    public MessageBuilder setTemplate(String template) {
        this.template = template;
        return this;
    }

    @Override
    public MessageBuilder setParams(Map<String, Object> params) {
        this.params = params;
        return this;
    }

    @Override
    public Message build() {
        if (this.content == null)
            this.content = VelocityEngineUtils
                    .mergeTemplateIntoString(velocityEngine, templatePath + template + templateSuffix, encoding, params);
        Message message = new Message();
        message.setAddress(this.address);
        message.setTitle(this.title);
        message.setContent(this.content);
        return message;
    }
}
