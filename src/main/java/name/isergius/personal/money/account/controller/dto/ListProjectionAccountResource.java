package name.isergius.personal.money.account.controller.dto;

import org.springframework.hateoas.ResourceSupport;

/**
 * Created by isergius on 28.04.16.
 */
public class ListProjectionAccountResource extends ResourceSupport {

    private Long accountId;
    private String title;

    public Long getAccountId() {
        return accountId;
    }

    public void setAccountId(Long accountId) {
        this.accountId = accountId;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }
}
