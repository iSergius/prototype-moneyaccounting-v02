package name.isergius.personal.money.account.controller.dto;

import org.springframework.hateoas.ResourceSupport;
import org.springframework.hateoas.core.Relation;

/**
 * Created by isergius on 28.04.16.
 */
@Relation(value = AccountResource.SINGLE_RELATION, collectionRelation = AccountResource.COLLECTION_RELATION)
public class AccountResource extends ResourceSupport {

    public static final String SINGLE_RELATION = "account";
    public static final String COLLECTION_RELATION = "accounts";

    private Long accountId;
    private String title;
    private boolean overdraft;
    private MoneyAmountResource value;

    public AccountResource() {
        super();
    }

    public Long getAccountId() {
        return accountId;
    }

    public void setAccountId(Long accountId) {
        this.accountId = accountId;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public boolean isOverdraft() {
        return overdraft;
    }

    public void setOverdraft(boolean overdraft) {
        this.overdraft = overdraft;
    }

    public MoneyAmountResource getValue() {
        return value;
    }

    public void setValue(MoneyAmountResource value) {
        this.value = value;
    }

    @Override
    public String toString() {
        return "AccountResource{" +
                "accountId=" + accountId +
                ", title='" + title + '\'' +
                ", overdraft=" + overdraft +
                ", value=" + value +
                '}';
    }
}
