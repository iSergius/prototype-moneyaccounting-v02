package name.isergius.personal.money.account.controller.dto;

import org.springframework.hateoas.ResourceSupport;

/**
 * Created by isergius on 21.04.16.
 */
public class ConsumeEntityResource<T> extends ResourceSupport {

    private T entity;

    public ConsumeEntityResource(T entity) {
        this.entity = entity;
    }

    public T getEntity() {
        return entity;
    }

    public void setEntity(T entity) {
        this.entity = entity;
    }
}
