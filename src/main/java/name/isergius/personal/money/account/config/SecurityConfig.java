package name.isergius.personal.money.account.config;

import name.isergius.personal.money.account.dao.RoleDao;
import name.isergius.personal.money.account.dao.UserDao;
import name.isergius.personal.money.account.domain.Role;
import name.isergius.personal.money.account.domain.User;
import name.isergius.personal.money.account.services.UserBuilderFactory;
import name.isergius.personal.money.account.services.UserService;
import name.isergius.personal.money.account.services.impl.DefaultUserBuilder;
import name.isergius.personal.money.account.services.impl.DefaultUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.springframework.core.annotation.Order;
import org.springframework.core.env.Environment;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.authentication.dao.DaoAuthenticationProvider;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.authentication.logout.HttpStatusReturningLogoutSuccessHandler;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.Properties;

/**
 * Created by isergius on 21.04.16.
 */

@Configuration
//@PropertySource(RootConfig.PROPERTIES)
@Import({RootConfig.class})
@EnableWebSecurity
public class SecurityConfig extends WebSecurityConfigurerAdapter {

    public static final String PROPERTY_DEFAULT_USER_ENABLED = "defaultUser.enabled";
    public static final String PROPERTY_DEFAULT_USER_ACCOUNT_NON_LOCKED = "defaultUser.accountNonLocked";
    public static final String PROPERTY_DEFAULT_USER_CONFIRMED = "defaultUser.confirmed";
    public static final String PROPERTY_DEFAULT_USER_ACCOUNT_EXPIRED = "defaultUser.accountExpired";
    public static final String PROPERTY_DEFAULT_USER_CREDENTIALS_EXPIRED = "defaultUser.credentialsExpired";
    public static final String PROPERTY_DEFAULT_USER_AGENT_TITLE = "defaultUser.agentTitle";
    public static final String PROPERTY_DEFAULT_USER_ROLE = "defaultUser.role";
    public static final String PROPERTY_DEFAULT_USER_ACCOUNT_TITLE = "defaultUser.accountTitle";
    public static final String PROPERTY_DEFAULT_USER_CURRENCY = "defaultUser.currency";
    public static final String PROPERTY_DEFAULT_USER_VALUE = "defaultUser.value";
    public static final String PROPERTY_DEFAULT_USER_OVERDRAFT = "defaultUser.overdraft";
    public static final String PROPERTY_ADMIN_ROLE = "admin.role";
    public static final String PROPERTY_ADMIN_USERNAME = "admin.username";
    public static final String PROPERTY_ADMIN_PASSWORD = "admin.password";
    public static final String PROPERTY_DEMO_USERNAME = "demo.username";
    public static final String PROPERTY_DEMO_PASSWORD = "demo.password";
    private static final String DEFAULT_USER_AGENT_TITLE = "";
    private static final String DEFAULT_USER_ACCOUNT_TITLE = "Cash";
    private static final String DEFAULT_USER_CURRENCY = "RUB";

    @Autowired
    private Environment environment;

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http.httpBasic()
                .and()
                .authorizeRequests()
                .antMatchers("/user/confirm/**","/user/register").permitAll()
                .antMatchers("/**").hasAnyRole("USER", "ADMIN")
                .and()
                .sessionManagement().sessionCreationPolicy(SessionCreationPolicy.STATELESS)
                .and()
                .csrf().disable()
                .headers().frameOptions().sameOrigin()
                .and()
                .logout().logoutSuccessHandler(logoutSuccessHandler());
    }

    @Autowired
    public void globalSecurity(AuthenticationManagerBuilder authenticationManagerBuilder,
                               AuthenticationProvider authenticationProvider,
                               UserDao userDao) throws Exception {
        authenticationManagerBuilder.authenticationProvider(authenticationProvider);
        authenticationManagerBuilder.userDetailsService(userDao);
    }

    @Order(1)
    @Autowired
    @Transactional
    public void createDefaultRoles(RoleDao roleDao) {
        Role roleAdmin = new Role(environment.getProperty(PROPERTY_ADMIN_ROLE));
        roleDao.save(roleAdmin);
        Role roleUser = new Role(environment.getProperty(PROPERTY_DEFAULT_USER_ROLE));
        roleDao.save(roleUser);
    }

    @Autowired
    public void createDefaultUsers(UserBuilderFactory userBuilderFactory, UserDao userDao) {
        User admin = userBuilderFactory.factory().setUsername(environment.getProperty(PROPERTY_ADMIN_USERNAME))
                .setPassword(environment.getProperty(PROPERTY_ADMIN_PASSWORD))
                .addRole(environment.getProperty(PROPERTY_ADMIN_ROLE))
                .setEnabled(true)
                .setAccountNonLocked(true)
                .setConfirmed(true)
                .build();
        userDao.save(admin);
        User user = userBuilderFactory.factory().setUsername(environment.getProperty(PROPERTY_DEMO_USERNAME))
                .setPassword(environment.getProperty(PROPERTY_DEMO_PASSWORD))
                .setEnabled(true)
                .setAccountNonLocked(true)
                .setConfirmed(true)
                .build();
        userDao.save(user);
    }

    @Bean
    public HttpStatusReturningLogoutSuccessHandler logoutSuccessHandler() {
        return new HttpStatusReturningLogoutSuccessHandler();
    }

    @Bean
    public AuthenticationProvider authenticationProvider(UserDao userDao) throws Exception {
        DaoAuthenticationProvider authenticationProvider = new DaoAuthenticationProvider();
        authenticationProvider.setUserDetailsService(userDao);
        authenticationProvider.setPasswordEncoder(passwordEncoder());
        authenticationProvider.afterPropertiesSet();
        return authenticationProvider;
    }

    @Bean
    public BCryptPasswordEncoder passwordEncoder() {
        return new BCryptPasswordEncoder();
    }

    @Bean
    public UserService userService(UserDao userDao, UserBuilderFactory userBuilderFactory) {
        return new DefaultUserService(userDao, userBuilderFactory);
    }

    @Bean
    public UserBuilderFactory userBuilderFactory(RoleDao roleDao, PasswordEncoder passwordEncoder) {
        Properties defaultProperties = new Properties();
        defaultProperties.put(DefaultUserBuilder.PROPERTY_ENABLED,
                environment.getProperty(PROPERTY_DEFAULT_USER_ENABLED, Boolean.class, true));
        defaultProperties.put(DefaultUserBuilder.PROPERTY_ACCOUNT_NON_LOCKED,
                environment.getProperty(PROPERTY_DEFAULT_USER_ACCOUNT_NON_LOCKED, Boolean.class, true));
        defaultProperties.put(DefaultUserBuilder.PROPERTY_CONFIRMED,
                environment.getProperty(PROPERTY_DEFAULT_USER_CONFIRMED, Boolean.class, false));
        String accountExpired = environment.getProperty(PROPERTY_DEFAULT_USER_ACCOUNT_EXPIRED);
        String credentialsExpired = environment.getProperty(PROPERTY_DEFAULT_USER_CREDENTIALS_EXPIRED);
        if (!accountExpired.isEmpty()) defaultProperties.put(DefaultUserBuilder.PROPERTY_ACCOUNT_EXPIRED,
                LocalDateTime.parse(accountExpired));
        if (!credentialsExpired.isEmpty()) defaultProperties.put(DefaultUserBuilder.PROPERTY_CREDENTIALS_EXPIRED,
                LocalDateTime.parse(credentialsExpired));
        defaultProperties.setProperty(DefaultUserBuilder.PROPERTY_AGENT_TITLE,
                environment.getProperty(PROPERTY_DEFAULT_USER_AGENT_TITLE, DEFAULT_USER_AGENT_TITLE));
        defaultProperties.setProperty(DefaultUserBuilder.PROPERTY_ROLE,
                environment.getProperty(PROPERTY_DEFAULT_USER_ROLE));
        defaultProperties.setProperty(DefaultUserBuilder.PROPERTY_ACCOUNT_TITLE,
                environment.getProperty(PROPERTY_DEFAULT_USER_ACCOUNT_TITLE, DEFAULT_USER_ACCOUNT_TITLE));
        defaultProperties.setProperty(DefaultUserBuilder.PROPERTY_CURRENCY,
                environment.getProperty(PROPERTY_DEFAULT_USER_CURRENCY, DEFAULT_USER_CURRENCY));
        String value = environment.getProperty(PROPERTY_DEFAULT_USER_VALUE);
        if (!value.isEmpty()) defaultProperties.put(DefaultUserBuilder.PROPERTY_VALUE,
                BigDecimal.valueOf(Long.valueOf(value)));
        else defaultProperties.put(DefaultUserBuilder.PROPERTY_VALUE,
                new BigDecimal(0L));
        defaultProperties.put(DefaultUserBuilder.PROPERTY_OVERDRAFT,
                environment.getProperty(PROPERTY_DEFAULT_USER_OVERDRAFT, Boolean.class, false));

        return new UserBuilderFactory(roleDao, passwordEncoder, defaultProperties);
    }

}
