package name.isergius.personal.money.account.controller.asm;

import name.isergius.personal.money.account.controller.TransactionController;
import name.isergius.personal.money.account.controller.dto.TransactionResource;
import org.springframework.hateoas.ResourceProcessor;

import static org.springframework.hateoas.mvc.ControllerLinkBuilder.linkTo;
import static org.springframework.hateoas.mvc.ControllerLinkBuilder.methodOn;

/**
 * Created by isergius on 23.04.16.
 */
public class TransactionProcessor implements ResourceProcessor<TransactionResource> {

    @Override
    public TransactionResource process(TransactionResource resource) {
        resource.add(linkTo(methodOn(TransactionController.class).commitTransaction(resource.getTransactionId())).withRel("commit"));
        resource.add(linkTo(methodOn(TransactionController.class).rollbackTransaction(resource.getTransactionId())).withRel("rollback"));
        return resource;
    }
}
