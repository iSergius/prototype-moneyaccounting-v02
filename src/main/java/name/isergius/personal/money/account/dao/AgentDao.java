package name.isergius.personal.money.account.dao;


import name.isergius.personal.money.account.domain.Agent;
import org.springframework.data.repository.PagingAndSortingRepository;

/**
 * Created by isergius on 28.03.16.
 */
public interface AgentDao extends PagingAndSortingRepository<Agent, Long> {
}
