package name.isergius.personal.money.account.controller.asm;

import name.isergius.personal.money.account.controller.dto.AccountResource;
import name.isergius.personal.money.account.domain.Account;
import org.springframework.hateoas.EntityLinks;
import org.springframework.hateoas.Resource;
import org.springframework.hateoas.ResourceProcessor;

/**
 * Created by isergius on 26.04.16.
 */
public class AccountProcessor implements ResourceProcessor<AccountResource> {

    @Override
    public AccountResource process(AccountResource resource) {
        return resource;
    }
}
