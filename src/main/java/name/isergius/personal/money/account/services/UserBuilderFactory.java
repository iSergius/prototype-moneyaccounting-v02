package name.isergius.personal.money.account.services;

import name.isergius.personal.money.account.dao.RoleDao;
import name.isergius.personal.money.account.services.impl.DefaultUserBuilder;
import org.springframework.security.crypto.password.PasswordEncoder;

import java.util.Properties;

/**
 * Created by isergius on 21.06.16.
 */
public class UserBuilderFactory {

    private final PasswordEncoder passwordEncoder;
    private final Properties defaultProperties;
    private RoleDao roleDao;

    public UserBuilderFactory(RoleDao roleDao,
                              PasswordEncoder passwordEncoder,
                              Properties defaultProperties) {
        this.roleDao = roleDao;
        this.passwordEncoder = passwordEncoder;
        this.defaultProperties = defaultProperties;
    }

    public UserBuilder factory() {
        return new DefaultUserBuilder(roleDao, passwordEncoder, defaultProperties);
    }
}
