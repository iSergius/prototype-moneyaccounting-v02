package name.isergius.personal.money.account.dao;


import name.isergius.personal.money.account.domain.Marker;
import org.springframework.data.repository.CrudRepository;

/**
 * Created by isergius on 28.03.16.
 */
public interface MarkerDao extends CrudRepository<Marker, Long> {
}
