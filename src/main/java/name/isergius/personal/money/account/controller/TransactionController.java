package name.isergius.personal.money.account.controller;


import name.isergius.personal.money.account.controller.asm.MoneyAmountResourceAssembler;
import name.isergius.personal.money.account.controller.asm.TransactionProcessor;
import name.isergius.personal.money.account.controller.asm.TransactionResourceAssembler;
import name.isergius.personal.money.account.controller.dto.ListProjectionAccountResource;
import name.isergius.personal.money.account.controller.dto.TransactionResource;
import name.isergius.personal.money.account.domain.Transaction;
import name.isergius.personal.money.account.services.TransactionBuilder;
import name.isergius.personal.money.account.services.TransactionService;
import name.isergius.personal.money.account.services.TransactionServiceFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PagedResourcesAssembler;
import org.springframework.hateoas.ExposesResourceFor;
import org.springframework.hateoas.MediaTypes;
import org.springframework.hateoas.PagedResources;
import org.springframework.http.*;
import org.springframework.web.bind.annotation.*;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Objects;

import static org.springframework.hateoas.mvc.ControllerLinkBuilder.linkTo;


/**
 * Created by isergius on 22.04.16.
 */
@RestController
@ExposesResourceFor(Transaction.class)
@RequestMapping(path = "/transaction", produces = MediaTypes.HAL_JSON_VALUE)
public class TransactionController {

    @Autowired
    private TransactionServiceFactory transactionServiceFactory;

    @Autowired
    private TransactionProcessor transactionProcessor;


    @RequestMapping(method = RequestMethod.GET)
    public HttpEntity<PagedResources<TransactionResource>> getTransactions(Pageable pageable,
                                                                           PagedResourcesAssembler<Transaction> assembler,
                                                                           TransactionResourceAssembler resourceAssembler) {
        HttpHeaders headers = new HttpHeaders();
        Page<Transaction> transactions = transactionServiceFactory.factory().findAll(pageable);
        PagedResources<TransactionResource> pagedResources = assembler.toResource(transactions, resourceAssembler);
        headers.setAllow(new HashSet<>(Arrays.asList(HttpMethod.GET, HttpMethod.POST)));
        return new ResponseEntity<>(pagedResources, headers, HttpStatus.OK);
    }

    @RequestMapping(path = "/{id}", method = RequestMethod.GET)
    public HttpEntity<TransactionResource> getTransaction(@PathVariable("id") Long id,
                                                          TransactionResourceAssembler resourceAssembler) {
        HttpHeaders headers = new HttpHeaders();
        Transaction transaction = transactionServiceFactory.factory().findOne(id);
        if (Objects.isNull(transaction)) return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        TransactionResource resource = resourceAssembler.toResource(transaction);
        transactionProcessor.process(resource);
        headers.setAllow(new HashSet<>(Arrays.asList(HttpMethod.GET, HttpMethod.DELETE)));
        return new ResponseEntity<>(resource, headers, HttpStatus.OK);
    }

    @RequestMapping(method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE)
    public HttpEntity createTransaction(@RequestBody TransactionResource resource,
                                        MoneyAmountResourceAssembler moneyAmountResourceAssembler) {
        TransactionService transactionService = transactionServiceFactory.factory();
        HttpHeaders headers = new HttpHeaders();
        ListProjectionAccountResource srcAccount = resource.getSrcAccount();
        ListProjectionAccountResource dstAccount = resource.getDstAccount();

        TransactionBuilder builder = transactionService.builder();
        builder.setDate(resource.getDate());
        if (srcAccount != null) builder.setSrcAccount(srcAccount.getAccountId());
        if (dstAccount != null) builder.setDstAccount(dstAccount.getAccountId());
        builder.setValue(moneyAmountResourceAssembler.toEntity(resource.getValue()));
        builder.setCommitted(resource.isCommitted());
        Transaction transaction = builder.build();
        try {

            transactionService.save(transaction);
        } catch (Exception e) {
            return new ResponseEntity(HttpStatus.BAD_REQUEST);
        }
        headers.setLocation(linkTo(this.getClass()).slash(transaction).toUri());
        headers.setContentType(MediaType.APPLICATION_JSON);
        return new ResponseEntity(headers, HttpStatus.CREATED);
    }

    @RequestMapping(path = "/{id}", method = RequestMethod.DELETE)
    public HttpEntity deleteTransaction(@PathVariable("id") Long id) {
        try {
            transactionServiceFactory.factory().delete(id);
        } catch (Exception e) {
            return new ResponseEntity(HttpStatus.METHOD_NOT_ALLOWED);
        }
        return new ResponseEntity(HttpStatus.NOT_FOUND);
    }

    @RequestMapping(path = "/commit", method = RequestMethod.GET)
    public HttpEntity<PagedResources<TransactionResource>> getCommittedTransactions(Pageable pageable,
                                                                                   PagedResourcesAssembler<Transaction> assembler,
                                                                                   TransactionResourceAssembler resourceAssembler) {
        Page<Transaction> transactions = transactionServiceFactory.factory().findCommitted(pageable);
        PagedResources<TransactionResource> pagedResources = assembler.toResource(transactions, resourceAssembler);
        return new ResponseEntity<>(pagedResources, HttpStatus.OK);
    }

    @RequestMapping(path = "/uncommit", method = RequestMethod.GET)
    public HttpEntity<PagedResources<TransactionResource>> getUncommittedTransaction(Pageable pageable,
                                                                                     PagedResourcesAssembler<Transaction> assembler,
                                                                                     TransactionResourceAssembler resourceAssembler) {
        Page<Transaction> transactions = transactionServiceFactory.factory().findUncommitted(pageable);
        PagedResources<TransactionResource> pagedResources = assembler.toResource(transactions, resourceAssembler);
        return new ResponseEntity<>(pagedResources, HttpStatus.OK);
    }

    @RequestMapping(path = "/{id}/commit", method = RequestMethod.POST)
    public HttpEntity commitTransaction(@PathVariable("id") Long id) {
        TransactionService transactionService = transactionServiceFactory.factory();
        if (transactionService.commit(id)) {
            return new ResponseEntity(HttpStatus.ACCEPTED);
        } else {
            return new ResponseEntity(HttpStatus.NOT_MODIFIED);
        }
    }

    @RequestMapping(path = "/{id}/rollback", method = RequestMethod.POST)
    public HttpEntity rollbackTransaction(@PathVariable("id") Long id) {
        TransactionService transactionService = transactionServiceFactory.factory();
        if (transactionService.rollback(id)) {
            return new ResponseEntity(HttpStatus.ACCEPTED);
        } else {
            return new ResponseEntity(HttpStatus.NOT_MODIFIED);
        }
    }

}