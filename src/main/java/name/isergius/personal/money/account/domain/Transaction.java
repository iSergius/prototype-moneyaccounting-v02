package name.isergius.personal.money.account.domain;

import name.isergius.personal.money.account.dao.Model;
import org.apache.log4j.Logger;

import javax.persistence.*;
import java.io.Serializable;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.Objects;

import static javax.persistence.CascadeType.*;

/**
 * Created by isergius on 21.03.16.
 */
@Entity
@Table(name = "TRANSACTIONS")
public class Transaction extends Model implements Serializable {

    private static final long serialVersionUID = -3255992754008909133L;

    private static final Logger log = Logger.getLogger(Transaction.class);

    private Agent agent;
    private Money value;
    private BigDecimal rate = new BigDecimal(1);
    private Account srcAccount;
    private Account dstAccount;
    private LocalDate date;
    private boolean committed;

    public Transaction() {
        super();
    }

    public Transaction(Money value,
                       Account srcAccount,
                       Account dstAccount,
                       LocalDate date,
                       boolean committed) {
        this.value = value;
        this.srcAccount = srcAccount;
        this.dstAccount = dstAccount;
        this.date = date;
        this.committed = committed;
    }

    public void commit() {
        if (committed) throw new TransactionException("Transaction already is committed");
        if (Objects.nonNull(srcAccount)) {
            srcAccount.outCome(value);
        }
        if (Objects.nonNull(dstAccount)) {
            dstAccount.inCome(exchange());
        }
        committed = true;
        log.trace("Committed " + this);
    }

    public void rollback() {
        if (!committed) throw new TransactionException("Transaction still is not committed");
        if (Objects.nonNull(srcAccount)) {
            srcAccount.inCome(value);
        }
        if (Objects.nonNull(dstAccount)) {
            dstAccount.outCome(exchange());
        }
        committed = false;
        log.info("Transaction " + this + " is rollback");
    }

    private Money exchange() {
        BigDecimal value = this.value.multiply(rate).getNumber().numberValue(BigDecimal.class);
        String currencyCode = dstAccount.getValue().getCurrencyCode();
        log.info("Exchange value: " + this.value.getAmount() + " " + this.value.getCurrencyCode()
                + " with rate: " + rate + " to: " + value + " currency: " + currencyCode);
        return new Money(value, currencyCode);
    }

    @ManyToOne
    public Agent getAgent() {
        return agent;
    }

    public void setAgent(Agent agent) {
        this.agent = agent;
    }

    @Embedded
    public Money getValue() {
        return value;
    }

    public void setValue(Money value) {
        this.value = value;
    }

    @Basic
    public BigDecimal getRate() {
        return rate;
    }

    public void setRate(BigDecimal rate) {
        this.rate = rate;
    }

    @ManyToOne(cascade = {REFRESH, MERGE, DETACH}, targetEntity = Account.class)
    public Account getSrcAccount() {
        return srcAccount;
    }

    public void setSrcAccount(Account srcAccount) {
        this.srcAccount = srcAccount;
    }

    @ManyToOne(cascade = {REFRESH, MERGE, DETACH}, targetEntity = Account.class)
    public Account getDstAccount() {
        return dstAccount;
    }

    public void setDstAccount(Account dstAccount) {
        this.dstAccount = dstAccount;
    }

    @Basic
    public LocalDate getDate() {
        return date;
    }

    public void setDate(LocalDate date) {
        this.date = date;
    }

    @Basic
    public boolean isCommitted() {
        return committed;
    }

    public void setCommitted(boolean committed) {
        this.committed = committed;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;
        Transaction that = (Transaction) o;
        return committed == that.committed &&
                Objects.equals(value, that.value) &&
                Objects.equals(srcAccount, that.srcAccount) &&
                Objects.equals(dstAccount, that.dstAccount) &&
                Objects.equals(date, that.date);
    }

    @Override
    public String toString() {
        return "Transaction{" +
                " id=" + getId() +
                ", value=" + value +
                ", rate=" + rate +
                ", srcAccount=" + srcAccount +
                ", dstAccount=" + dstAccount +
                ", date=" + date +
                ", committed=" + committed +
                '}';
    }
}
