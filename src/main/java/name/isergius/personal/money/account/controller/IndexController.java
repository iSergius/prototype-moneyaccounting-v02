package name.isergius.personal.money.account.controller;


import name.isergius.personal.money.account.controller.dto.AccountResource;
import name.isergius.personal.money.account.controller.dto.TransactionResource;
import org.springframework.hateoas.MediaTypes;
import org.springframework.hateoas.ResourceSupport;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import static org.springframework.hateoas.mvc.ControllerLinkBuilder.linkTo;
import static org.springframework.hateoas.mvc.ControllerLinkBuilder.methodOn;

/**
 * Created by isergius on 21.04.16.
 */
@RestController
@RequestMapping(produces = MediaTypes.HAL_JSON_VALUE)
public class IndexController {

    @RequestMapping(path = "/", method = RequestMethod.GET)
    public ResponseEntity<?> rootIndex() {
        ResourceSupport resource = new ResourceSupport();
        resource.add(linkTo(methodOn(IndexController.class).rootIndex()).withSelfRel());
        resource.add(linkTo(methodOn(IndexController.class).homeIndex()).withRel("home"));
        return ResponseEntity.ok(resource);
    }

    @RequestMapping(path = "/home", method = RequestMethod.GET)
    public ResponseEntity<?> homeIndex() {
        ResourceSupport resource = new ResourceSupport();
        resource.add(linkTo(methodOn(IndexController.class).homeIndex()).withSelfRel());
        resource.add(linkTo(AccountController.class).withRel(AccountResource.COLLECTION_RELATION));
        resource.add(linkTo(TransactionController.class).withRel(TransactionResource.COLLECTION_RELATION));
        return ResponseEntity.ok(resource);
    }

    @RequestMapping(path = "/rel/{rel}", method = RequestMethod.GET)
    public ResponseEntity<?> relIndex(@PathVariable("rel") String rel) {
        return ResponseEntity.ok("OK " + rel);
    }

}
