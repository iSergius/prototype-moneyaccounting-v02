package name.isergius.personal.money.account.services.impl;

import name.isergius.personal.money.account.domain.Account;
import name.isergius.personal.money.account.domain.Money;
import name.isergius.personal.money.account.domain.Transaction;
import name.isergius.personal.money.account.services.AccountServiceFactory;
import name.isergius.personal.money.account.services.TransactionBuilder;

import java.time.LocalDate;

/**
 * Created by isergius on 28.04.16.
 */
public class DefaultTransactionBuilder implements TransactionBuilder {

    private AccountServiceFactory accountServiceFactory;

    private Account srcAccount;
    private Account dstAccount;
    private Money value;
    private LocalDate date;
    private boolean committed;

    public DefaultTransactionBuilder(AccountServiceFactory accountServiceFactory) {
        this.accountServiceFactory = accountServiceFactory;
    }

    @Override
    public TransactionBuilder setSrcAccount(Long srcAccountId) {
        if (srcAccount != null) this.srcAccount = accountServiceFactory.factory().findOne(srcAccountId);
        return this;
    }

    @Override
    public TransactionBuilder setDstAccount(Long dstAccountId) {
        if (dstAccountId != null) this.dstAccount = accountServiceFactory.factory().findOne(dstAccountId);
        return this;
    }

    @Override
    public TransactionBuilder setValue(Money value) {
        this.value = value;
        return this;
    }

    @Override
    public TransactionBuilder setDate(LocalDate date) {
        this.date = date;
        return this;
    }

    @Override
    public TransactionBuilder setCommitted(boolean commited) {
        this.committed = commited;
        return this;
    }

    @Override
    public Transaction build() {
        return new Transaction(value, srcAccount, dstAccount, date, committed);
    }
}
