package name.isergius.personal.money.account.controller.asm;

import name.isergius.personal.money.account.controller.TransactionController;
import name.isergius.personal.money.account.controller.dto.TransactionResource;
import name.isergius.personal.money.account.domain.Transaction;
import org.springframework.hateoas.mvc.ResourceAssemblerSupport;

/**
 * Created by isergius on 28.04.16.
 */
public class TransactionResourceAssembler extends ResourceAssemblerSupport<Transaction, TransactionResource> {

    private ListProjectionAccountResourceAssembler accountResourceAssembler = new ListProjectionAccountResourceAssembler();
    private MoneyAmountResourceAssembler moneyAmountResourceAssembler = new MoneyAmountResourceAssembler();

    public TransactionResourceAssembler() {
        super(TransactionController.class, TransactionResource.class);
    }

    @Override
    public TransactionResource toResource(Transaction entity) {
        TransactionResource resource = createResourceWithId(entity.getId(), entity);
        resource.setTransactionId(entity.getId());
        resource.setCommitted(entity.isCommitted());
        resource.setDate(entity.getDate());
        resource.setDstAccount(accountResourceAssembler.toResource(entity.getDstAccount()));
        resource.setSrcAccount(accountResourceAssembler.toResource(entity.getSrcAccount()));
        resource.setValue(moneyAmountResourceAssembler.toResource(entity.getValue()));
        return resource;
    }
}
