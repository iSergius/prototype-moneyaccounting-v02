package name.isergius.personal.money.account.domain;

import name.isergius.personal.money.account.dao.Model;
import org.apache.log4j.Logger;

import javax.persistence.Basic;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.Table;
import java.io.Serializable;
import java.util.Objects;

/**
 * Created by isergius on 21.03.16.
 */
@Entity
@Table(name = "ACCOUNTS")
public class Account extends Model implements Serializable {

    private static final long serialVersionUID = -5096750564769852318L;

    private static final Logger log = Logger.getLogger(Account.class);

    private String title;
    private boolean overdraft;
    private Money value;

    public Account() {}

    public Account(String title, boolean overdraft, Money value) {
        this.title = title;
        this.overdraft = overdraft;
        this.value = value;
    }

    public void inCome(Money value) {
        this.value = (Money) this.value.add(value);
        log.info("Income " + value.getAmount() + " " + value.getCurrencyCode()
                + " and value is " + this.value.getAmount() + " " + this.value.getCurrencyCode());
    }

    public void outCome(Money value) {
        this.value = (Money) this.value.subtract(value);
        log.info("Outcome " + value.getAmount() + " " + value.getCurrencyCode()
                + " and value is " + this.value.getAmount() + " " + this.value.getCurrencyCode());
    }

    @Basic
    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    @Basic
    public boolean isOverdraft() {
        return overdraft;
    }

    public void setOverdraft(boolean overdraft) {
        this.overdraft = overdraft;
    }

    @Embedded
    public Money getValue() {
        return value;
    }

    public void setValue(Money value) {
        this.value = value;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;
        Account account = (Account) o;
        return overdraft == account.overdraft &&
                Objects.equals(title, account.title) &&
                Objects.equals(value, account.value);
    }

    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode());
    }

    @Override
    public String toString() {
        return "Account{" +
                "title='" + title + '\'' +
                ", overdraft=" + overdraft +
                ", value=" + value +
                '}';
    }
}
