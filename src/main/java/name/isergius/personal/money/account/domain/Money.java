package name.isergius.personal.money.account.domain;

import javax.money.*;
import javax.persistence.*;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Arrays;
import java.util.Objects;
import java.util.stream.Collectors;

/**
 * Created by isergius on 01.04.16.
 */
@Embeddable
@Inheritance(strategy = InheritanceType.TABLE_PER_CLASS)
public class Money implements MonetaryAmount, Comparable<MonetaryAmount>, Serializable, CurrencySupplier {

    private static final long serialVersionUID = 1084949984024568736L;

    private BigDecimal amount = BigDecimal.ZERO;
    private String currencyCode;
    private MonetaryAmount monetaryAmount;

    public Money() {
    }

    public Money(MonetaryAmount amount) {
        this(amount.getNumber().numberValue(BigDecimal.class), amount.getCurrency().getCurrencyCode());
    }

    public Money(BigDecimal amount, String currencyCode) {
        this.amount = amount;
        this.currencyCode = currencyCode;
        constructMonetaryAmount();
    }

    @Basic
    public BigDecimal getAmount() {
        return amount;
    }

    public void setAmount(BigDecimal amount) {
        this.amount = amount;
        constructMonetaryAmount();
    }

    @Basic
    public String getCurrencyCode() {
        return currencyCode;
    }

    public void setCurrencyCode(String currencyCode) {
        this.currencyCode = currencyCode;
        constructMonetaryAmount();
    }

    @PostLoad
    private void constructMonetaryAmount() {
        if (Objects.nonNull(currencyCode))
            monetaryAmount = Monetary.getDefaultAmountFactory()
                    .setCurrency(currencyCode)
                    .setNumber(amount)
                    .create();
    }


    @Transient
    @Override
    public MonetaryContext getContext() {
        return monetaryAmount.getContext();
    }

    @Transient
    @Override
    public MonetaryAmountFactory<? extends MonetaryAmount> getFactory() {
        return monetaryAmount.getFactory();
    }

    @Override
    public boolean isGreaterThan(MonetaryAmount amount) {
        return monetaryAmount.isGreaterThan(amount);
    }

    @Override
    public boolean isGreaterThanOrEqualTo(MonetaryAmount amount) {
        return monetaryAmount.isGreaterThanOrEqualTo(amount);
    }

    @Override
    public boolean isLessThan(MonetaryAmount amount) {
        return monetaryAmount.isLessThan(amount);
    }

    @Override
    public boolean isLessThanOrEqualTo(MonetaryAmount amt) {
        return monetaryAmount.isLessThanOrEqualTo(amt);
    }

    @Override
    public boolean isEqualTo(MonetaryAmount amount) {
        return monetaryAmount.isEqualTo(amount);
    }

    @Override
    public int signum() {
        return monetaryAmount.signum();
    }

    @Override
    public MonetaryAmount add(MonetaryAmount amount) {
        return new Money(monetaryAmount.add(amount));
    }

    @Override
    public MonetaryAmount subtract(MonetaryAmount amount) {
        return new Money(monetaryAmount.subtract(amount));
    }

    @Override
    public MonetaryAmount multiply(long multiplicand) {
        return new Money(monetaryAmount.multiply(multiplicand));
    }

    @Override
    public MonetaryAmount multiply(double multiplicand) {
        return new Money(monetaryAmount.multiply(multiplicand));
    }

    @Override
    public MonetaryAmount multiply(java.lang.Number multiplicand) {
        return new Money(monetaryAmount.multiply(multiplicand));
    }

    @Override
    public MonetaryAmount divide(long divcurrencyCoder) {
        return new Money(monetaryAmount.divide(divcurrencyCoder));
    }

    @Override
    public MonetaryAmount divide(double divcurrencyCoder) {
        return new Money(monetaryAmount.divide(divcurrencyCoder));
    }

    @Override
    public MonetaryAmount divide(java.lang.Number divcurrencyCoder) {
        return new Money(monetaryAmount.divide(divcurrencyCoder));
    }

    @Override
    public MonetaryAmount remainder(long divcurrencyCoder) {
        return new Money(monetaryAmount.remainder(divcurrencyCoder));
    }

    @Override
    public MonetaryAmount remainder(double divcurrencyCoder) {
        return new Money(monetaryAmount.remainder(divcurrencyCoder));
    }

    @Override
    public MonetaryAmount remainder(java.lang.Number divcurrencyCoder) {
        return new Money(monetaryAmount.remainder(divcurrencyCoder));
    }

    @Override
    public MonetaryAmount[] divideAndRemainder(long divcurrencyCoder) {
        return (MonetaryAmount[]) Arrays.asList(monetaryAmount.divideAndRemainder(divcurrencyCoder)).stream()
                .map(Money::new)
                .collect(Collectors.toList())
                .toArray();
    }

    @Override
    public MonetaryAmount[] divideAndRemainder(double divcurrencyCoder) {
        return (MonetaryAmount[]) Arrays.asList(monetaryAmount.divideAndRemainder(divcurrencyCoder)).stream()
                .map(Money::new)
                .collect(Collectors.toList())
                .toArray();
    }

    @Override
    public MonetaryAmount[] divideAndRemainder(java.lang.Number divcurrencyCoder) {
        return (MonetaryAmount[]) Arrays.asList(monetaryAmount.divideAndRemainder(divcurrencyCoder)).stream()
                .map(Money::new)
                .collect(Collectors.toList())
                .toArray();
    }

    @Override
    public MonetaryAmount divideToIntegralValue(long divcurrencyCoder) {
        return new Money(monetaryAmount.divideToIntegralValue(divcurrencyCoder));
    }

    @Override
    public MonetaryAmount divideToIntegralValue(double divcurrencyCoder) {
        return new Money(monetaryAmount.divideToIntegralValue(divcurrencyCoder));
    }

    @Override
    public MonetaryAmount divideToIntegralValue(java.lang.Number divcurrencyCoder) {
        return new Money(monetaryAmount.divideToIntegralValue(divcurrencyCoder));
    }

    @Override
    public MonetaryAmount scaleByPowerOfTen(int power) {
        return new Money(monetaryAmount.scaleByPowerOfTen(power));
    }

    @Override
    public MonetaryAmount abs() {
        return new Money(monetaryAmount.abs());
    }

    @Override
    public MonetaryAmount negate() {
        return new Money(monetaryAmount.negate());
    }

    @Override
    public MonetaryAmount plus() {
        return new Money(monetaryAmount.plus());
    }

    @Override
    public MonetaryAmount stripTrailingZeros() {
        return new Money(monetaryAmount.stripTrailingZeros());
    }

    @Override
    public int compareTo(MonetaryAmount o) {
        return monetaryAmount.compareTo(o);
    }

    @Transient
    @Override
    public CurrencyUnit getCurrency() {
        return monetaryAmount.getCurrency();
    }

    @Transient
    @Override
    public NumberValue getNumber() {
        return monetaryAmount.getNumber();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Money that = (Money) o;
        return Objects.equals(amount, that.amount) &&
                Objects.equals(currencyCode, that.currencyCode);
    }

    @Override
    public int hashCode() {
        return Objects.hash(amount, currencyCode);
    }

    @Override
    public String toString() {
        return "Money{" +
                "amount=" + amount +
                ", currencyCode='" + currencyCode + '\'' +
                ", monetaryAmount=" + monetaryAmount +
                '}';
    }
}
