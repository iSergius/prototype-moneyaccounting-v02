package name.isergius.personal.money.account.services.impl;


import name.isergius.personal.money.account.dao.TransactionDao;
import name.isergius.personal.money.account.domain.Transaction;
import name.isergius.personal.money.account.domain.TransactionException;
import name.isergius.personal.money.account.services.TransactionBuilder;
import name.isergius.personal.money.account.services.TransactionBuilderFactory;
import name.isergius.personal.money.account.services.TransactionService;
import org.apache.log4j.Logger;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

/**
 * Created by isergius on 25.04.16.
 */
@Transactional(isolation = Isolation.READ_COMMITTED, propagation = Propagation.REQUIRES_NEW)
public class DefaultTransactionService implements TransactionService {

    private static final Logger log = Logger.getLogger(DefaultTransactionService.class);

    private TransactionDao transactionDao;
    private TransactionBuilderFactory transactionBuilderFactory;

    public DefaultTransactionService(TransactionDao transactionDao, TransactionBuilderFactory transactionBuilderFactory) {
        this.transactionDao = transactionDao;
        this.transactionBuilderFactory = transactionBuilderFactory;
    }

    @Override
    public Page<Transaction> findAll(Pageable pageable) {
        return transactionDao.findAll(pageable);
    }

    @Override
    public Transaction findOne(Long id) {
        return transactionDao.findOne(id);
    }

    @Override
    public void save(Transaction transaction) {
        transactionDao.save(transaction);
    }

    @Override
    public void delete(Long id) {
        transactionDao.delete(id);
    }

    @Override
    public TransactionBuilder builder() {
        return transactionBuilderFactory.factory();
    }

    @Override
    public Page<Transaction> findCommitted(Pageable pageable) {
        return transactionDao.committedTrue(pageable);
    }

    @Override
    public Page<Transaction> findUncommitted(Pageable pageable) {
        return transactionDao.committedFalse(pageable);
    }

    @Override
    public boolean commit(Long id) {
        Transaction transaction = transactionDao.findOne(id);
        try {
            transaction.commit();
            transactionDao.save(transaction);
        } catch (TransactionException e) {
            log.warn("Warn transaction " + transaction + " could not committed");
            return false;
        }
        log.info("Transaction " + transaction + " committed");
        return true;
    }

    @Override
    public boolean rollback(Long id) {
        Transaction transaction = transactionDao.findOne(id);
        try {
            transaction.rollback();
            transactionDao.save(transaction);
        } catch (TransactionException e) {
            log.warn("Warn transaction " + transaction + " could not rollback");
            return false;
        }
        log.info("Transaction " + transaction + " rollback");
        return true;
    }

}
