package name.isergius.personal.money.account.dao;


import name.isergius.personal.money.account.domain.User;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;

/**
 * Created by isergius on 28.03.16.
 */
public interface UserDao extends PagingAndSortingRepository<User, Long>, UserDetailsService {

    @Override
    @Query("SELECT c FROM User c WHERE c.username = :username")
    User loadUserByUsername(@Param("username") String username) throws UsernameNotFoundException;

    User secretIs(String secret);

    @Override
    @Query("SELECT u FROM User u WHERE u.id = :id")
    User findOne(@Param("id") Long aLong);
}
