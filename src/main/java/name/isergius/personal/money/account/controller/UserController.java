package name.isergius.personal.money.account.controller;

import name.isergius.personal.money.account.controller.asm.UserResourceAssembler;
import name.isergius.personal.money.account.controller.dto.UserForm;
import name.isergius.personal.money.account.controller.dto.UserResource;
import name.isergius.personal.money.account.domain.Message;
import name.isergius.personal.money.account.domain.User;
import name.isergius.personal.money.account.services.MessageService;
import name.isergius.personal.money.account.services.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.hateoas.ExposesResourceFor;
import org.springframework.hateoas.MediaTypes;
import org.springframework.http.*;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;

import static org.springframework.hateoas.mvc.ControllerLinkBuilder.linkTo;
import static org.springframework.hateoas.mvc.ControllerLinkBuilder.methodOn;

/**
 * Created by isergius on 16.06.16.
 */
@RestController
@ExposesResourceFor(User.class)
@RequestMapping(path = "/user", produces = MediaTypes.HAL_JSON_VALUE)
public class UserController {

    @Autowired
    private UserService userService;
    @Autowired
    private MessageService messageService;

    @RequestMapping(method = RequestMethod.GET)
    public HttpEntity<UserResource> currentUser(UserResourceAssembler resourceAssembler) {
        HttpHeaders headers = new HttpHeaders();
        User user = userService.getCurrentUser();
        UserResource userResource = resourceAssembler.toResource(user);
        headers.setAllow(new HashSet<>(Arrays.asList(HttpMethod.GET, HttpMethod.POST)));
        return new ResponseEntity<>(userResource, headers, HttpStatus.OK);
    }

    @RequestMapping(path = "/register", method = RequestMethod.GET)
    public HttpEntity<UserForm> registerUserDto() {
        return new ResponseEntity<>(new UserForm(), HttpStatus.OK);
    }

    @RequestMapping(path = "/register", method = RequestMethod.POST)
    public HttpEntity registerUser(@RequestBody @Valid UserForm userForm) {
        User user = userService.builder()
                .setUsername(userForm.getUsername())
                .setPassword(userForm.getPassword())
                .build();
        userService.save(user);
        Map<String, Object> mailParams = new HashMap<>();
        String confirmationLink = linkTo(methodOn(UserController.class).confirmRegistration(user.getSecret()))
                .toUri().toString();
        mailParams.put("link", confirmationLink);
        Message message = messageService.builder()
                .setAddress(userForm.getUsername())
                .setTitle("Welcome!")
                .setTemplate("ConfirmNewUser")
                .setParams(mailParams)
                .build();
        messageService.send(message);
        return new ResponseEntity(HttpStatus.NO_CONTENT);
    }

    @RequestMapping(path = "/confirm/{secret}", method = RequestMethod.GET)
    public HttpEntity confirmRegistration(@PathVariable("secret") String secret) {
        if (userService.confirmRegistration(secret))
            return new ResponseEntity(HttpStatus.ACCEPTED);
        else
            return new ResponseEntity(HttpStatus.NOT_FOUND);
    }
}
