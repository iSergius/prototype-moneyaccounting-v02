package name.isergius.personal.money.account.services;

import name.isergius.personal.money.account.config.SecurityConfig;
import name.isergius.personal.money.account.dao.TransactionDao;
import name.isergius.personal.money.account.services.impl.DefaultTransactionService;
import name.isergius.personal.money.account.services.impl.PersonalTransactionService;
import org.apache.log4j.Logger;
import org.springframework.core.env.Environment;


/**
 * Created by isergius on 28.06.16.
 */
public class TransactionServiceFactory {

    private static final Logger log = Logger.getLogger(TransactionServiceFactory.class);

    private final UserService userService;
    private final TransactionBuilderFactory transactionBuilderFactory;
    private final TransactionDao transactionDao;
    private final Environment environment;

    public TransactionServiceFactory(TransactionDao transactionDao,
                                     TransactionBuilderFactory transactionBuilderFactory,
                                     UserService userService,
                                     Environment environment) {
        this.transactionDao = transactionDao;
        this.transactionBuilderFactory = transactionBuilderFactory;
        this.userService = userService;
        this.environment = environment;
        log.trace("Created TransactionServiceFactory");
    }

    public TransactionService factory() {
        if (userService.currentUserIsContainRole(environment.getProperty(SecurityConfig.PROPERTY_ADMIN_ROLE))) {
            log.info("Factory DefaultTransactionService");
            return new DefaultTransactionService(transactionDao, transactionBuilderFactory);
        } else {
            log.info("Factory PersonalTransactionService");
            return new PersonalTransactionService(transactionDao, transactionBuilderFactory, userService);
        }
    }
}
