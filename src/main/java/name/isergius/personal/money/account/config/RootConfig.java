package name.isergius.personal.money.account.config;

import name.isergius.personal.money.account.dao.AccountDao;
import name.isergius.personal.money.account.dao.MessageDao;
import name.isergius.personal.money.account.dao.TransactionDao;
import name.isergius.personal.money.account.services.*;
import name.isergius.personal.money.account.services.impl.*;
import name.isergius.personal.money.account.services.messag.DefaultMessageBuilder;
import name.isergius.personal.money.account.services.messag.MailMessageDeliveryService;
import name.isergius.personal.money.account.services.messag.MessageBuilderFactory;
import name.isergius.personal.money.account.services.messag.MessageDeliveryService;
import org.apache.velocity.app.VelocityEngine;
import org.apache.velocity.exception.VelocityException;
import org.springframework.context.annotation.*;
import org.springframework.core.env.Environment;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.JavaMailSenderImpl;
import org.springframework.ui.velocity.VelocityEngineFactoryBean;

import java.io.IOException;
import java.util.List;
import java.util.Properties;

/**
 * Created by isergius on 21.04.16.
 */
@Configuration
@PropertySource(RootConfig.PROPERTIES)
@Import({DataConfig.class})
public class RootConfig {

    public static final String PROPERTIES = "classpath:application.properties";

    public static final String PROPERTY_MAIL_HOST = "mail.host";
    public static final String PROPERTY_MAIL_PORT = "mail.port";
    public static final String PROPERTY_MAIL_USERNAME = "mail.username";
    public static final String PROPERTY_MAIL_PASSWORD = "mail.password";
    public static final String PROPERTY_MAIL_DEFAULT_TITLE = "mail.defaultTitle";
    public static final String PROPERTY_MAIL_ENCODING = "mail.encoding";
    public static final String PROPERTY_MAIL_TEMPLATE_PATH = "mail.templatePath";
    public static final String PROPERTY_MAIL_TEMPLATE_SUFFIX = "mail.templateSuffix";
    private static final String DEFAULT_MAIL_PORT = "25";
    private static final String ENV_HOSTNAME = "HOSTNAME";
    private static final String ENV_USERNAME = "USERNAME";
    private static final String MAIL_DEFAULT_TITLE = "";
    private static final String MAIL_ENCODING = "UTF-8";
    private static final String MAIL_TEMPLATE_PATH = "/";
    private static final String MAIL_TEMPLATE_SUFFIX = "";

    @Bean
    public TransactionServiceFactory transactionService(TransactionDao transactionDao,
                                                        TransactionBuilderFactory transactionBuilderFactory,
                                                        UserService userService,
                                                        Environment environment) {
        return new TransactionServiceFactory(transactionDao, transactionBuilderFactory, userService, environment);
    }

    @Bean
    public AccountServiceFactory accountService(AccountDao accountDao,
                                                UserService userService,
                                                Environment environment) {
        return new AccountServiceFactory(accountDao, userService, environment);
    }

    @Bean
    public TransactionBuilderFactory transactionBuilder(AccountServiceFactory accountServiceFactory) {
        return new TransactionBuilderFactory(accountServiceFactory);
    }

    @Bean
    public MessageService messagingService(MessageBuilderFactory messageBuilderFactory,
                                           List<MessageDeliveryService> messageDeliveryServices,
                                           MessageDao messageDao) {
        return new DefaultMessageService(messageBuilderFactory, messageDeliveryServices, messageDao);
    }

    @Bean
    public MailMessageDeliveryService mailMessageDeliveryService(Environment environment,
                                                                 JavaMailSender mailSender) {
        return new MailMessageDeliveryService(environment, mailSender);
    }

    @Bean
    public JavaMailSender mailSender(Environment environment) {
        JavaMailSenderImpl javaMailSender = new JavaMailSenderImpl();
        javaMailSender.setHost(environment.getProperty(PROPERTY_MAIL_HOST, System.getenv(ENV_HOSTNAME)));
        javaMailSender.setPort(Integer.valueOf(environment.getProperty(PROPERTY_MAIL_PORT, DEFAULT_MAIL_PORT)));
        javaMailSender.setUsername(environment.getProperty(PROPERTY_MAIL_USERNAME, System.getenv(ENV_USERNAME)));
        javaMailSender.setPassword(environment.getProperty(PROPERTY_MAIL_PASSWORD));
        return javaMailSender;
    }

    @Bean
    public VelocityEngine velocityEngine() throws VelocityException, IOException {
        VelocityEngineFactoryBean factory = new VelocityEngineFactoryBean();
        Properties props = new Properties();
        props.put("resource.loader", "class");
        props.put("class.resource.loader.class", "org.apache.velocity.runtime.resource.loader.ClasspathResourceLoader");
        factory.setVelocityProperties(props);

        return factory.createVelocityEngine();
    }

    @Bean
    public MessageBuilderFactory messageBuilderFactory(VelocityEngine velocityEngine, Environment environment) {
        Properties defaultProperties = new Properties();
        defaultProperties.setProperty(DefaultMessageBuilder.PROPERTY_TITLE,
                environment.getProperty(PROPERTY_MAIL_DEFAULT_TITLE, MAIL_DEFAULT_TITLE));
        defaultProperties.setProperty(DefaultMessageBuilder.PROPERTY_ENCODING,
                environment.getProperty(PROPERTY_MAIL_ENCODING, MAIL_ENCODING));
        defaultProperties.setProperty(DefaultMessageBuilder.PROPERTY_TEMPLATE_PATH,
                environment.getProperty(PROPERTY_MAIL_TEMPLATE_PATH, MAIL_TEMPLATE_PATH));
        defaultProperties.setProperty(DefaultMessageBuilder.PROPERTY_TEMPLATE_SUFFIX,
                environment.getProperty(PROPERTY_MAIL_TEMPLATE_SUFFIX, MAIL_TEMPLATE_SUFFIX));
        return new MessageBuilderFactory(velocityEngine, defaultProperties);
    }

}
