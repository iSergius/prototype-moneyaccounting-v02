package name.isergius.personal.money.account.controller.asm;

import name.isergius.personal.money.account.controller.UserController;
import name.isergius.personal.money.account.controller.dto.UserResource;
import name.isergius.personal.money.account.domain.User;
import org.springframework.hateoas.mvc.ResourceAssemblerSupport;

/**
 * Created by isergius on 16.06.16.
 */
public class UserResourceAssembler extends ResourceAssemblerSupport<User, UserResource> {

    private AccountResourceAssembler accountResourceAssembler = new AccountResourceAssembler();

    public UserResourceAssembler() {
        super(UserController.class, UserResource.class);
    }

    @Override
    public UserResource toResource(User entity) {
        UserResource userResource = new UserResource();
        userResource.setAccountResources(accountResourceAssembler.toResources(entity.getAgent().getAccounts()));
        return userResource;
    }
}
