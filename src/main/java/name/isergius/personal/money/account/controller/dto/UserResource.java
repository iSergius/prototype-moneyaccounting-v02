package name.isergius.personal.money.account.controller.dto;

import org.springframework.hateoas.ResourceSupport;

import java.util.List;

/**
 * Created by isergius on 16.06.16.
 */
public class UserResource extends ResourceSupport {

    private List<AccountResource> accountResources;

    public List<AccountResource> getAccountResources() {
        return accountResources;
    }

    public void setAccountResources(List<AccountResource> accountResources) {
        this.accountResources = accountResources;
    }
}
