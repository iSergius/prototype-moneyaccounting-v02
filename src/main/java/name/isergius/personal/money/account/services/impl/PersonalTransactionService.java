package name.isergius.personal.money.account.services.impl;

import name.isergius.personal.money.account.dao.TransactionDao;
import name.isergius.personal.money.account.domain.Transaction;
import name.isergius.personal.money.account.domain.TransactionException;
import name.isergius.personal.money.account.services.TransactionBuilder;
import name.isergius.personal.money.account.services.TransactionBuilderFactory;
import name.isergius.personal.money.account.services.TransactionService;
import name.isergius.personal.money.account.services.UserService;
import org.apache.log4j.Logger;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

/**
 * Created by isergius on 27.06.16.
 */
@Transactional(propagation = Propagation.REQUIRES_NEW, isolation = Isolation.REPEATABLE_READ)
public class PersonalTransactionService implements TransactionService {

    private static final Logger log = Logger.getLogger(PersonalTransactionService.class);

    private final TransactionDao transactionDao;
    private final TransactionBuilderFactory transactionBuilderFactory;
    private final UserService userService;

    public PersonalTransactionService(TransactionDao transactionDao,
                                      TransactionBuilderFactory builder,
                                      UserService userService) {
        this.transactionDao = transactionDao;
        this.transactionBuilderFactory = builder;
        this.userService = userService;
        log.trace("Create PersonalTransactionService");
    }

    @Override
    public Page<Transaction> findAll(Pageable pageable) {
        return transactionDao.findAllPersonal(pageable);
    }

    @Override
    public Transaction findOne(Long id) {
        return transactionDao.findOnePersonal(id);
    }

    @Override
    public void save(Transaction transaction) {
        transaction.setAgent(userService.getCurrentUser().getAgent());
        transactionDao.save(transaction);
        log.debug("save transaction: " + transaction);
    }

    @Override
    public void delete(Long id) {
        transactionDao.deletePersonal(id);
    }

    @Override
    public TransactionBuilder builder() {
        return transactionBuilderFactory.factory();
    }

    @Override
    public Page<Transaction> findCommitted(Pageable pageable) {
        return transactionDao.committedTruePersonal(pageable);
    }

    @Override
    public Page<Transaction> findUncommitted(Pageable pageable) {
        return transactionDao.committedFalsePersonal(pageable);
    }

    @Override
    public boolean commit(Long id) {
        Transaction transaction = transactionDao.findOnePersonal(id);
        log.trace("Obtain " + transaction);
        try {
            transaction.commit();
            transactionDao.save(transaction);
            log.debug("Transaction " + transaction.getId() + " is saved");
        } catch (TransactionException e) {
            log.warn("Warn transaction " + transaction.getId() + " could not committed");
            return false;
        }
        log.info("Transaction " + transaction.getId() + " successful committed");
        return true;
    }

    @Override
    public boolean rollback(Long id) {
        Transaction transaction = transactionDao.findOnePersonal(id);
        try {
            transaction.rollback();
            transactionDao.save(transaction);
        } catch (TransactionException e) {
            log.warn("Warn transaction " + transaction + " could not rollback");
            return false;
        }
        log.info("Transaction " + transaction + " rollback");
        return true;
    }
}
