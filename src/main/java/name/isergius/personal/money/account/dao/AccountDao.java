package name.isergius.personal.money.account.dao;

import name.isergius.personal.money.account.domain.Account;
import org.springframework.data.repository.PagingAndSortingRepository;

/**
 * Created by isergius on 23.04.16.
 */
public interface AccountDao extends PagingAndSortingRepository<Account, Long> {
}
