package name.isergius.personal.money.account.services.messag;

import name.isergius.personal.money.account.domain.Message;
import org.springframework.core.env.Environment;
import org.springframework.mail.MailException;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;

/**
 * Created by isergius on 18.06.16.
 */
public class MailMessageDeliveryService implements MessageDeliveryService {

    private Environment environment;
    private JavaMailSender mailSender;

    public MailMessageDeliveryService(Environment environment, JavaMailSender mailSender) {
        this.environment = environment;
        this.mailSender = mailSender;
    }

    @Override
    public void delivery(Message message) {
        if (message.getRetries() > Integer.valueOf(environment.getProperty("mail.retries"))) return;
        try {
            mailSender.send(mimeMessage -> {
                MimeMessageHelper messageHelper = new MimeMessageHelper(mimeMessage);
                messageHelper.setFrom(environment.getProperty("mail.sender"));
                messageHelper.setTo(message.getAddress());
                messageHelper.setSubject(message.getTitle());
                messageHelper.setText(message.getContent(), true);
            });
            message.setTransmitted(true);
        } catch (MailException e) {
            message.setRetries(message.getRetries() + 1);
            message.setError(e.getMessage());
        }
    }
}
