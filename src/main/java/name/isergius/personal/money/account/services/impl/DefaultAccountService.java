package name.isergius.personal.money.account.services.impl;

import name.isergius.personal.money.account.dao.AccountDao;
import name.isergius.personal.money.account.domain.Account;
import name.isergius.personal.money.account.services.AccountService;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

/**
 * Created by isergius on 26.04.16.
 */
@Transactional(isolation = Isolation.READ_COMMITTED, propagation = Propagation.REQUIRES_NEW)
public class DefaultAccountService implements AccountService {

    private AccountDao accountDao;

    public DefaultAccountService(AccountDao accountDao) {
        this.accountDao = accountDao;
    }

    @Override
    public Page<Account> findAll(Pageable pageable) {
        return accountDao.findAll(pageable);
    }

    @Override
    public Account findOne(Long id) {
        return accountDao.findOne(id);
    }

    @Override
    public void save(Account transaction) {
        accountDao.save(transaction);
    }

    @Override
    public void delete(Long id) {
        accountDao.delete(id);
    }
}
