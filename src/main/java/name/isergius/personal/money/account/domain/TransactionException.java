package name.isergius.personal.money.account.domain;

/**
 * Created by isergius on 02.05.16.
 */
public class TransactionException extends RuntimeException {

    public TransactionException(String message) {
        super(message);
    }
}
