package name.isergius.personal.money.account.services;

import name.isergius.personal.money.account.domain.Transaction;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

/**
 * Created by isergius on 26.04.16.
 */
public interface TransactionService {

    Page<Transaction> findAll(Pageable pageable);

    Transaction findOne(Long id);

    void save(Transaction transaction);

    void delete(Long id);

    TransactionBuilder builder();

    Page<Transaction> findCommitted(Pageable pageable);

    Page<Transaction> findUncommitted(Pageable pageable);

    boolean commit(Long id);

    boolean rollback(Long id);
}
