package name.isergius.personal.money.account.services;

import name.isergius.personal.money.account.domain.User;

/**
 * Created by isergius on 16.06.16.
 */
public interface UserService {

    User getCurrentUser();

    UserBuilder builder();

    boolean confirmRegistration(String secret);

    void save(User user);

    boolean currentUserIsContainRole(String role);
}
