package name.isergius.personal.money.account.config;

import org.springframework.security.web.context.AbstractSecurityWebApplicationInitializer;

/**
 * Created by isergius on 07.06.16.
 */
public class SecurityInit extends AbstractSecurityWebApplicationInitializer {
}
