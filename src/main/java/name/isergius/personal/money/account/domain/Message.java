package name.isergius.personal.money.account.domain;

import name.isergius.personal.money.account.dao.Model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import java.util.Objects;

/**
 * Created by isergius on 17.06.16.
 */
@Entity
@Table(name = "MESSAGES")
public class Message extends Model {

    private String address;
    private String title;
    private String content;
    private boolean transmitted;
    private int retries;
    private String error;


    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    @Column(length = 2000)
    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public boolean isTransmitted() {
        return transmitted;
    }

    public void setTransmitted(boolean transmitted) {
        this.transmitted = transmitted;
    }

    public int getRetries() {
        return retries;
    }

    public void setRetries(int retries) {
        this.retries = retries;
    }

    public String getError() {
        return error;
    }

    public void setError(String error) {
        this.error = error;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;
        Message message = (Message) o;
        return transmitted == message.transmitted &&
                retries == message.retries &&
                Objects.equals(address, message.address) &&
                Objects.equals(title, message.title) &&
                Objects.equals(content, message.content) &&
                Objects.equals(error, message.error);
    }

    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(), address, title, content, transmitted, retries, error);
    }

    @Override
    public String toString() {
        return "Message{" +
                "address='" + address + '\'' +
                ", title='" + title + '\'' +
                ", content='" + content + '\'' +
                ", transmitted=" + transmitted +
                ", retries=" + retries +
                ", error='" + error + '\'' +
                '}';
    }
}
