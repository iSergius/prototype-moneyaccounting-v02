package name.isergius.personal.money.account.controller.dto;

import org.springframework.hateoas.ResourceSupport;

import java.math.BigDecimal;

/**
 * Created by isergius on 04.05.16.
 */
public class MoneyAmountResource extends ResourceSupport {

    private BigDecimal amount;
    private String currencyCode;


    public BigDecimal getAmount() {
        return amount;
    }

    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }

    public String getCurrencyCode() {
        return currencyCode;
    }

    public void setCurrencyCode(String currencyCode) {
        this.currencyCode = currencyCode;
    }
}
