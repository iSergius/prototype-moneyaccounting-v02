package name.isergius.personal.money.account.services.impl;

import name.isergius.personal.money.account.domain.Account;
import name.isergius.personal.money.account.domain.Agent;
import name.isergius.personal.money.account.domain.User;
import name.isergius.personal.money.account.services.AccountService;
import name.isergius.personal.money.account.services.UserService;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

/**
 * Created by isergius on 26.06.16.
 */
@Transactional(propagation = Propagation.REQUIRES_NEW, isolation = Isolation.REPEATABLE_READ)
public class PersonalAccountService implements AccountService {

    private final UserService userService;

    public PersonalAccountService(UserService userService) {
        this.userService = userService;
    }

    @Override
    public Page<Account> findAll(Pageable pageable) {
        Agent agent = userService.getCurrentUser().getAgent();
        return new PageImpl<>(agent.getAccounts(), pageable, agent.getAccounts().size());
    }

    @Override
    public Account findOne(Long id) {
        List<Account> accounts = userService.getCurrentUser().getAgent().getAccounts();
        Optional<Account> first = accounts.stream().filter(account -> account.getId().equals(id)).findFirst();
        return first.get();
    }

    @Override
    public void save(Account account) {
        User currentUser = userService.getCurrentUser();
        currentUser.getAgent().addAccount(account);
        userService.save(currentUser);
    }

    @Override
    public void delete(Long id) {
        User currentUser = userService.getCurrentUser();
        List<Account> accounts = currentUser.getAgent().getAccounts();
        Optional<Account> first = accounts.stream().filter(account -> account.getId().equals(id)).findFirst();
        accounts.remove(first);
        userService.save(currentUser);
    }
}
