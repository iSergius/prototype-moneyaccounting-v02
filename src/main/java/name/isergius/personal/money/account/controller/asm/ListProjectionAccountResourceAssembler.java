package name.isergius.personal.money.account.controller.asm;

import name.isergius.personal.money.account.controller.AccountController;
import name.isergius.personal.money.account.controller.dto.ListProjectionAccountResource;
import name.isergius.personal.money.account.domain.Account;
import org.springframework.hateoas.mvc.ResourceAssemblerSupport;

/**
 * Created by isergius on 28.04.16.
 */
public class ListProjectionAccountResourceAssembler extends ResourceAssemblerSupport<Account, ListProjectionAccountResource> {

    public ListProjectionAccountResourceAssembler() {
        super(AccountController.class, ListProjectionAccountResource.class);
    }

    @Override
    public ListProjectionAccountResource toResource(Account entity) {
        if (entity == null) return null;
        ListProjectionAccountResource resource = createResourceWithId(entity.getId(), entity);
        resource.setAccountId(entity.getId());
        resource.setTitle(entity.getTitle());
        return resource;
    }
}
