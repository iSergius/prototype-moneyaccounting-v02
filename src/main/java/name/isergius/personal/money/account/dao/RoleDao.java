package name.isergius.personal.money.account.dao;


import name.isergius.personal.money.account.domain.Role;
import org.springframework.data.repository.PagingAndSortingRepository;

/**
 * Created by isergius on 05.04.16.
 */
public interface RoleDao extends PagingAndSortingRepository<Role, Long> {
        Role findByTitle(String role);
}
