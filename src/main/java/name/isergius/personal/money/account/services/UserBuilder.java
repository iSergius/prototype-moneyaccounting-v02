package name.isergius.personal.money.account.services;

import name.isergius.personal.money.account.domain.User;

import java.math.BigDecimal;
import java.time.LocalDateTime;

/**
 * Created by isergius on 21.06.16.
 */
public interface UserBuilder {

    UserBuilder setEnabled(boolean enabled);

    UserBuilder setAccountNonLocked(boolean accountNonLocked);

    UserBuilder setConfirmed(boolean confirmed);

    UserBuilder setAccountExpired(LocalDateTime accountExpired);

    UserBuilder setCredentialsExpired(LocalDateTime credentialsExpired);

    UserBuilder setSecret(String secret);

    UserBuilder setAgentTitle(String agentTitle);

    UserBuilder setUsername(String username);

    UserBuilder setPassword(String password);

    UserBuilder addAccount(String title, BigDecimal value, String currency, boolean overdraft);

    UserBuilder addRole(String title);

    User build();
}
