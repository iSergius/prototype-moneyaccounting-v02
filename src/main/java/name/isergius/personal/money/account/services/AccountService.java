package name.isergius.personal.money.account.services;

import name.isergius.personal.money.account.domain.Account;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

/**
 * Created by isergius on 26.04.16.
 */
public interface AccountService {
    Page<Account> findAll(Pageable pageable);

    Account findOne(Long id);

    void save(Account transaction);

    void delete(Long id);
}
