package name.isergius.personal.money.account.domain;

import name.isergius.personal.money.account.dao.Model;

import javax.persistence.Basic;
import javax.persistence.Entity;
import javax.persistence.ManyToMany;
import javax.persistence.Table;
import java.io.Serializable;
import java.util.List;
import java.util.Objects;

/**
 * Created by isergius on 21.03.16.
 */
@Entity
@Table(name = "MARKERS")
public class Marker extends Model implements Serializable {

    private static final long serialVersionUID = 873226776024170525L;

    private String title;
    private List<Transaction> records;

    @Basic
    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    @ManyToMany
    public List<Transaction> getRecords() {
        return records;
    }

    public void setRecords(List<Transaction> records) {
        this.records = records;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;
        Marker marker = (Marker) o;
        return Objects.equals(title, marker.title) &&
                Objects.equals(records, marker.records);
    }

    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(), title, records);
    }

    @Override
    public String toString() {
        return "Marker{" +
                "title='" + title + '\'' +
                ", records=" + records +
                '}';
    }
}
