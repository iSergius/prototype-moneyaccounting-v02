package name.isergius.personal.money.account.controller;

import name.isergius.personal.money.account.controller.asm.AccountProcessor;
import name.isergius.personal.money.account.controller.asm.AccountResourceAssembler;
import name.isergius.personal.money.account.controller.dto.AccountResource;
import name.isergius.personal.money.account.domain.Account;
import name.isergius.personal.money.account.services.AccountServiceFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PagedResourcesAssembler;
import org.springframework.hateoas.ExposesResourceFor;
import org.springframework.hateoas.MediaTypes;
import org.springframework.hateoas.PagedResources;
import org.springframework.http.*;
import org.springframework.web.bind.annotation.*;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Objects;

import static org.springframework.hateoas.mvc.ControllerLinkBuilder.linkTo;

/**
 * Created by isergius on 23.04.16.
 */
@RestController
@ExposesResourceFor(Account.class)
@RequestMapping(path = "/account", produces = MediaTypes.HAL_JSON_VALUE)
public class AccountController {

    @Autowired
    private AccountServiceFactory accountServiceFactory;

    @Autowired
    private AccountProcessor accountProcessor;

    @RequestMapping(method = RequestMethod.GET)
    public HttpEntity<PagedResources<AccountResource>> getAccounts(Pageable pageable,
                                                                   PagedResourcesAssembler<Account> assembler,
                                                                   AccountResourceAssembler resourceAssembler) {
        HttpHeaders headers = new HttpHeaders();
        Page<Account> accounts = accountServiceFactory.factory().findAll(pageable);
        PagedResources<AccountResource> pagedResources = assembler.toResource(accounts, resourceAssembler);
        headers.setAllow(new HashSet<>(Arrays.asList(HttpMethod.GET, HttpMethod.POST)));
        return new ResponseEntity<>(pagedResources, headers, HttpStatus.OK);
    }

    @RequestMapping(path = "/{id}", method = RequestMethod.GET)
    public HttpEntity<AccountResource> getAccount(@PathVariable("id") Long id,
                                                  AccountResourceAssembler resourceAssembler) {
        HttpHeaders headers = new HttpHeaders();
        Account account = accountServiceFactory.factory().findOne(id);
        if (Objects.isNull(account)) return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        AccountResource resource = resourceAssembler.toResource(account);
        accountProcessor.process(resource);
        headers.setAllow(new HashSet<>(Arrays.asList(HttpMethod.GET, HttpMethod.DELETE)));
        return new ResponseEntity<>(resource, headers, HttpStatus.OK);
    }

    @RequestMapping(method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE)
    public HttpEntity createAccount(@RequestBody AccountResource accountResource, AccountResourceAssembler assembler) {
        HttpHeaders headers = new HttpHeaders();
        Account account = assembler.toEntity(accountResource);
        try {
            accountServiceFactory.factory().save(account);
        } catch (Exception e) {
            return new ResponseEntity(HttpStatus.BAD_REQUEST);
        }
        headers.setLocation(linkTo(this.getClass()).slash(account).toUri());
        headers.setContentType(MediaType.APPLICATION_JSON);
        return new ResponseEntity(headers, HttpStatus.CREATED);
    }

    @RequestMapping(path = "/{id}", method = RequestMethod.DELETE)
    public HttpEntity deleteAccount(@PathVariable("id") Long id) {
        try {
            accountServiceFactory.factory().delete(id);
        } catch (Exception e) {
            return new ResponseEntity(HttpStatus.METHOD_NOT_ALLOWED);
        }
        return new ResponseEntity(HttpStatus.NOT_FOUND);
    }
}
