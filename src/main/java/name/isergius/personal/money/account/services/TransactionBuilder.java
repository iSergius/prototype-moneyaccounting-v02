package name.isergius.personal.money.account.services;

import name.isergius.personal.money.account.domain.Money;
import name.isergius.personal.money.account.domain.Transaction;

import java.time.LocalDate;

/**
 * Created by isergius on 28.04.16.
 */
public interface TransactionBuilder {

    TransactionBuilder setSrcAccount(Long srcAccountId);
    TransactionBuilder setDstAccount(Long dstAccountId);
    TransactionBuilder setValue(Money value);
    TransactionBuilder setDate(LocalDate date);
    TransactionBuilder setCommitted(boolean flag);
    Transaction build();
}
