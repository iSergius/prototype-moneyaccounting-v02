package name.isergius.personal.money.account.controller.asm;

import name.isergius.personal.money.account.controller.AccountController;
import name.isergius.personal.money.account.controller.dto.AccountResource;
import name.isergius.personal.money.account.domain.Account;
import org.springframework.hateoas.mvc.ResourceAssemblerSupport;

/**
 * Created by isergius on 28.04.16.
 */
public class AccountResourceAssembler extends ResourceAssemblerSupport<Account, AccountResource> {

    private MoneyAmountResourceAssembler moneyAmountResourceAssembler = new MoneyAmountResourceAssembler();

    public AccountResourceAssembler() {
        super(AccountController.class, AccountResource.class);
    }

    @Override
    public AccountResource toResource(Account entity) {
        if (entity == null) return null;
        AccountResource resource = createResourceWithId(entity.getId(), entity);
        resource.setAccountId(entity.getId());
        resource.setOverdraft(entity.isOverdraft());
        resource.setTitle(entity.getTitle());
        resource.setValue(moneyAmountResourceAssembler.toResource(entity.getValue()));
        return resource;
    }

    public Account toEntity(AccountResource resource) {
        Account entity = new Account();
        entity.setOverdraft(resource.isOverdraft());
        entity.setTitle(resource.getTitle());
        entity.setValue(moneyAmountResourceAssembler.toEntity(resource.getValue()));
        return entity;
    }
}
