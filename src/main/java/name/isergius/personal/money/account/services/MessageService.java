package name.isergius.personal.money.account.services;

import name.isergius.personal.money.account.domain.Message;
import name.isergius.personal.money.account.services.messag.MessageBuilder;

/**
 * Created by isergius on 17.06.16.
 */
public interface MessageService {

    void send(Message message);

    void transmitAll();

    MessageBuilder builder();
}
