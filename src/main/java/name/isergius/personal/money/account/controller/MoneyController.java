package name.isergius.personal.money.account.controller;

import name.isergius.personal.money.account.domain.Money;
import org.springframework.hateoas.ExposesResourceFor;
import org.springframework.hateoas.MediaTypes;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * Created by isergius on 04.05.16.
 */
@RestController
@ExposesResourceFor(Money.class)
@RequestMapping(path = "/money",produces = MediaTypes.HAL_JSON_VALUE)
public class MoneyController {
}
