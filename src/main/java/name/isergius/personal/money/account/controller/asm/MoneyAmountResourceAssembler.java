package name.isergius.personal.money.account.controller.asm;

import name.isergius.personal.money.account.controller.MoneyController;
import name.isergius.personal.money.account.controller.dto.MoneyAmountResource;
import name.isergius.personal.money.account.domain.Money;
import org.springframework.hateoas.mvc.ResourceAssemblerSupport;

/**
 * Created by isergius on 04.05.16.
 */
public class MoneyAmountResourceAssembler extends ResourceAssemblerSupport<Money, MoneyAmountResource> {

    public MoneyAmountResourceAssembler() {
        super(MoneyController.class, MoneyAmountResource.class);
    }

    @Override
    public MoneyAmountResource toResource(Money entity) {
        MoneyAmountResource resource = new MoneyAmountResource();
        resource.setAmount(entity.getAmount());
        resource.setCurrencyCode(entity.getCurrency().getCurrencyCode());
        return resource;
    }

    public Money toEntity(MoneyAmountResource resource) {
        return new Money(resource.getAmount(), resource.getCurrencyCode());
    }
}
