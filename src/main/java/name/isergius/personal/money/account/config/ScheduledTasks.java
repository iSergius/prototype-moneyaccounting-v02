package name.isergius.personal.money.account.config;

import name.isergius.personal.money.account.services.MessageService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;

/**
 * Created by isergius on 19.06.16.
 */
@Configuration
@EnableScheduling
@Import(RootConfig.class)
public class ScheduledTasks {

    @Autowired
    private MessageService messageService;

    @Scheduled(fixedDelay = 5 * 60 * 1000)
    public void messagingTask() {
        messageService.transmitAll();
    }
}
