package name.isergius.personal.money.account.services.messag;

import name.isergius.personal.money.account.domain.Message;

import java.util.Map;

/**
 * Created by isergius on 27.06.16.
 */
public interface MessageBuilder {

    MessageBuilder setAddress(String address);

    MessageBuilder setTitle(String title);

    MessageBuilder setContent(String content);

    MessageBuilder setTemplate(String template);

    MessageBuilder setParams(Map<String, Object> params);

    Message build();
}
