package name.isergius.personal.money.account.services;

import name.isergius.personal.money.account.services.impl.DefaultTransactionBuilder;

/**
 * Created by isergius on 01.05.16.
 */
public class TransactionBuilderFactory {

    private AccountServiceFactory accountServiceFactory;

    public TransactionBuilderFactory(AccountServiceFactory accountServiceFactory) {
        this.accountServiceFactory = accountServiceFactory;
    }

    public TransactionBuilder factory() {
        return new DefaultTransactionBuilder(accountServiceFactory);
    }
}
